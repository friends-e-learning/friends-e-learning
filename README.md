# Friends e-Learning


**Welcome to "Friends e-Learning" system!**

Our application is a learning system, where you can initially:
- Register as new user
- Log-in
- Log-out

If you are not logged-in, you can only see our public courses, but you can't open them.
But if you are one of our **students**:

- You can see the courses you are enrolled in (both private and public). Private courses are visible only if you are enrolled and can’t be accessed in any other way (such as manually typing in the URL).
- You are able to see available public courses and if you visit one, you are automatically enrolled in it (on first visit).
- You can see all the sections of the courses you are enrolled, but if the section is still restricted, you are able to see only the title and the date, when it will be unlocked.
- You can see an unenroll button that removes you from the course and navigates back to the main courses page.
- You can edit and delete your own profile.

If you are one of our **teachers**:

- You are able to see all courses.
- You can create a new course with title, description and public/private check.
- You can change the status of the course to public/private at any time.
- You can create sections with HTML content.
- You can drag and drop sections if you want to change their order.
- You can select the date when the section will be unlocked for the students and you are able to change it at any time.
- If the course is private, you can enroll and unenroll students.
- You are provided with a way to search for students that are currently not enrolled in the current course.
- You can delete any user

To install all the dependencies in our application you should run:

- **npm install**

You can run the server with: **npm run start:dev**

You can run the app with: **npm start**

In order to login as a teacher, you could choose one of the following accounts:

e-mail: tania@gmail.com; 
password: 123456

e-mail: lyuben79@abv.bg;
password: lyuben

You can find the schema in the file **Friends e-Learning.sql** in the schema folder. To connect with the database, please enter your personal password for MariaDB in the file **config.js**
