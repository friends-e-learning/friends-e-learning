import serviceError from './service-error.js';
import coursesData from './../data/courses-data.js';
import { DEFAULT_USER_ROLE } from '../config.js';

/*eslint no-unused-vars: "off"*/

const getAllSections = sectionsData => {
    return async (id, role, user,courses_id) => {
        
        const result = await coursesData.getBy('id', id);
        
        if (! result) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                sections: null,
            };
        }
        const courseSections = await sectionsData.getSections(id);

        return {
            error: null,
            sections: courseSections
        };
    };
};

const getSectionById = sectionsData => {
    return async (id, course_id, role, user, courses_id) => {
        const enrolledIds = await coursesData.getEnrolledStudents(course_id, courses_id);
        const enrolledEmail = enrolledIds.map(el => el.email);
        
        const result = await coursesData.getBy('id', +course_id);
       
        if (role === DEFAULT_USER_ROLE && result?.isPublic === 0 && !(enrolledEmail.includes(user.email))){
            return {
                error: serviceError.OPERATION_NOT_PERMITTED,
                course: null
            }; 
        }
 
        if (! result) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                sections: null,
            };
        }
        const section = await sectionsData.getBy('id', +id, +course_id);
        
        return { error: null, sections: section };
    };
};

const createSection = (sectionsData) => 
    async (section, id) => {
        const { title, content, order, availableFrom } = section;
        const course = await coursesData.getBy('id', +id);
        if (! course) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                data: null,
            };
        }
  
        const createdSection = await sectionsData.create(title, content, order, availableFrom, +id);

        return {
            error: null,
            data: createdSection,
        };
    };

const updateSection = sectionsData => {
    return async (id, updateData, courseId, role) => {
        const course = await coursesData.getBy('id', +courseId);
        if (!course) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                section: null
            };
        }

        const section = await sectionsData.getBy('id', +id, +courseId);
        if (!section || section.courses_id !== courseId) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                section: null
            };
        }
        if (role === DEFAULT_USER_ROLE ){
            return { 
                error: serviceError.OPERATION_NOT_PERMITTED, 
                section: null 
            };
        }
    
        const updated = { ...section, ...updateData };
        const _ = await sectionsData.update(updated);
    
        return { error: null, section: updated };
    };
};

const deleteSection = sectionsData => {
    return async (id, courseId, role) => {
        const result = await coursesData.getBy('id', courseId);
        if (! result) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                data: null,
            };
        }
        const sectionToDelete = await sectionsData.getBy('id', id);
        if (!sectionToDelete) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                section: null
            };
        }
        if(role === DEFAULT_USER_ROLE ){
           
            return{ error: serviceError.OPERATION_NOT_PERMITTED, section: null };
            
        }

        const _ = await sectionsData.remove(sectionToDelete);

        return { error: null, section: sectionToDelete };
    };
};

const updateSectionByOrderUp = sectionsData => {
    return async (id, updateData, courseId, role) => {
        const course = await coursesData.getBy('id', +courseId);
        if (!course) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                section: null
            };
        }

        const section = await sectionsData.getBy('id', +id, +courseId);
        if (!section || section.courses_id !== courseId) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                section: null
            };
        }
        if (role === DEFAULT_USER_ROLE ){
            return { 
                error: serviceError.OPERATION_NOT_PERMITTED, 
                section: null 
            };
        }
    
        const updated = { ...section, ...updateData };
     
        const _ = await sectionsData.updateSectionsOrderUp(updated);
    
        return { error: null, section: updated };
    };
};
const updateSectionByOrderDown = sectionsData => {
    return async (id, updateData, courseId, role) => {
        const course = await coursesData.getBy('id', +courseId);
        if (!course) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                section: null
            };
        }

        const section = await sectionsData.getBy('id', +id, +courseId);
        if (!section || section.courses_id !== courseId) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                section: null
            };
        }
        if (role === DEFAULT_USER_ROLE ){
            return { 
                error: serviceError.OPERATION_NOT_PERMITTED, 
                section: null 
            };
        }
    
        const updated = { ...section, ...updateData };
        
        const _ = await sectionsData.updateSectionsOrderDown(updated);
    
        return { error: null, section: updated };
    };
};

export default {
    getAllSections,
    getSectionById,
    createSection,
    updateSection,
    deleteSection,
    updateSectionByOrderUp,
    updateSectionByOrderDown
};