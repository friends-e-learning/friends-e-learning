import serviceErrors from './service-error.js';
import bcrypt from 'bcrypt';
import { DEFAULT_USER_ROLE } from './../config.js';

/*eslint no-unused-vars: "off"*/

const signInUser = usersData => {
    return async (email, password) => {
        const user = await usersData.getWithRole(email);

        if (!user || !(await bcrypt.compare(password, user.password)) || !email) {
            return {
                error: serviceErrors.INVALID_SIGNIN,
                user: null
            };
        }

        return {
            error: null,
            user: user
        };
    };
};

const getAllUsers = usersData => {
    return async (filter) => {
        return filter
            ? await usersData.searchBy('email', filter)
            : await usersData.getAll();
    };
};

const getUserById = usersData => {
    return async (id) => {
        const user = await usersData.getBy('id', id);

        if (!user) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                user: null
            };
        }

        return { error: null, user: user };
    };
};

const createUser = usersData => {
    return async (userCreate) => {
        const { email, firstName, lastName, password } = userCreate;

        const existingUser = await usersData.getBy('email', email);

        if (existingUser) {
            return {
                error: serviceErrors.DUPLICATE_RECORD,
                user: null
            };
        }

        const passwordHash = await bcrypt.hash(password, 10);
        const user = await usersData.create(email, firstName, lastName, passwordHash, DEFAULT_USER_ROLE);

        return { error: null, user: user };
    };
};

const updateUser = usersData => {
    return async (id, userId, userUpdate, role) => {
        const user = await usersData.getBy('id', id);

        if (!user) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                user: null
            };
        }
        if (role === DEFAULT_USER_ROLE ){
            if (user.id !== userId ) {
                return { error: serviceErrors.OPERATION_NOT_PERMITTED, user: null };
            }
        } 

        const users = await usersData.getAll();
        
        const updated = { ...user, ...userUpdate };
        const _ = await usersData.update(updated);

        return { error: null, user: updated };
    };
};

const deleteUser = usersData => {
    return async (id, userId, role) => {
        const userToDelete = await usersData.getBy('id', id);
        if (!userToDelete) {
            return {
                error: serviceErrors.RECORD_NOT_FOUND,
                user: null
            };
        }
        if (role === DEFAULT_USER_ROLE ){
            if (userToDelete.id !== userId ){
                return { error: serviceErrors.OPERATION_NOT_PERMITTED, user: null };
            }
        }

        const _ = await usersData.remove(userToDelete);

        return { error: null, user: userToDelete };
    };
};

const getEnrolledCoursesId = usersData => {
    return async (id) => {
        const enrolledCourses = await usersData.getStudentsEnrolledCourses(id);

        return {
            error: null,
            courses: enrolledCourses
        };
    };
};
export default {
    signInUser,
    getAllUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser,
    getEnrolledCoursesId
};