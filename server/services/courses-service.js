import { DEFAULT_USER_ROLE } from '../config.js';
import serviceError from './service-error.js';
/*eslint no-unused-vars: "off"*/

const getAllCourses = courseData => {
    return async (filter) => {
        return filter
            ? await courseData.searchBy('title', filter)
            : await courseData.getAll();
    };
};

const getAllPublicCourses = courseData => {
    return async (filter) => {
        return filter
            ? await courseData.searchBy('title', filter)
            : await courseData.getAllPublic();
    };
};

const getCourseById = courseData => {
    return async (id, role, user, courses_id ) => {
        const enrolledIds = await courseData.getEnrolledStudents(id, courses_id);
        const enrolledEmail = enrolledIds.map(el => el.email);
        
        const course = await courseData.getBy('id', id);
        if (role === DEFAULT_USER_ROLE && course?.isPublic === 0 && !(enrolledEmail.includes(user.email))){
            return {
                error: serviceError.OPERATION_NOT_PERMITTED,
                course: null
            }; 
        }

        if (!course) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                course: null
            };
        }

        return { error: null, course: course };
    };
};

const createCourse = (courseData) => {
    return async (course, role) => {
        const { title, description, isPublic } = course;

        if (role === DEFAULT_USER_ROLE ) {
            return { error: serviceError.OPERATION_NOT_PERMITTED, course: null };
        }
        const existingCourse = await courseData.getBy('title', title);

        if (existingCourse) {
            return {
                error: serviceError.DUPLICATE_RECORD,
                course: null
            };
        }
        const createdCourse = await courseData.create(title, description, isPublic);

        return {
            error: null,
            course: createdCourse,
        };
    };
};

const updateCourse = courseData => {
    return async (id, updateData, role) => {
        const course = await courseData.getBy('id', id);
        if (!course) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                course: null
            };
        }
        if(role === DEFAULT_USER_ROLE ){
            
            return{ error: serviceError.OPERATION_NOT_PERMITTED, course: null };
        }

        const updated = { ...course, ...updateData };
        const _ = await courseData.update(updated);

        return { error: null, course: updated };
    };
};

const updateCourseToPublic = courseData => {
    return async (id, role) => {
        const courseToPublic = await courseData.getBy('id', id);
        if (!courseToPublic) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                course: null
            };
        }
        if (role === DEFAULT_USER_ROLE ) {
            
            return { error: serviceError.OPERATION_NOT_PERMITTED, courseToPublic: null };
        }

        const _ = await courseData.setPublic(courseToPublic);

        return { error: null, course: courseToPublic };
    };
};
const updateCourseToPrivate = courseData => {
    return async (id, role) => {
        const courseToPrivate = await courseData.getBy('id', id);
        if (!courseToPrivate) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                course: null
            };
        }
        if (role === DEFAULT_USER_ROLE ) {
            
            return { error: serviceError.OPERATION_NOT_PERMITTED, course: null };
        }

        const _ = await courseData.setPrivate(courseToPrivate);

        return { error: null, course: courseToPrivate };
    };
};

const deleteCourse = courseData => {
    return async (id, role) => {
        const courseToDelete = await courseData.getBy('id', id);
        if (!courseToDelete) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                course: null
            };
        }
        if(role === DEFAULT_USER_ROLE ){
           
            return{ error: serviceError.OPERATION_NOT_PERMITTED, course: null };
        }
        
        const _ = await courseData.remove(courseToDelete);

        return { error: null, course: courseToDelete };
    };
};

const getEnrolledStudentsId = courseData => {

    return async (courses_id, id) => {
        const result = await courseData.getBy('id', id);
        if (! result) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                data: null,
            };
        }
        const enrolledIds = await courseData.getEnrolledStudents(id, courses_id);
        
        return {
            error: null,
            data: enrolledIds
        };
    };
};

const getNotEnrolledStudentsId = courseData => {
    return async (courses_id, id) => {
        const result = await courseData.getBy('id', id);
        if (! result) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                data: null,
            };
        }
        const notEnrolledIds = await courseData.getNotEnrolledStudents(id, courses_id);
        
        return {
            error: null,
            data: notEnrolledIds
        };
    };
};

const enrollCourse = courseData => {
    return async (courses_id, users_id) => {
        const course = await courseData.getBy('id', courses_id);
        if (!course) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                course: null
            };
        }

        const _ = await courseData.getByIds(courses_id, users_id);

        const result = await courseData.enroll(courses_id, users_id);
       
        return {
            error: null,
            course: result,
        };
    };
};
const enrollCoursebyEmail = courseData => {
    return async (courses_id, users_id) => {
        const course = await courseData.getBy('id', courses_id);
        if (!course) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                course: null
            };
        }

        const _ = await courseData.getByIds(courses_id, users_id);

        const result = await courseData.postenrollByEmail(courses_id, users_id);
       
        return {
            error: null,
            course: result,
        };
    };
};

const unEnrollCourse = courseData => {
    return async (courses_id, users_id) => {
        const course = await courseData.getBy('id', courses_id);
        if (!course) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                course: null
            };
        }

        const _ = await courseData.getByIds(courses_id, users_id);

        const result = await courseData.unEnroll(courses_id, users_id);
       
        return {
            error: null,
            course: result,
        };
    };
};

const unEnrollCourseByEmail = courseData => {
    return async (courses_id, users_id) => {
        const course = await courseData.getBy('id', courses_id);
        if (!course) {
            return {
                error: serviceError.RECORD_NOT_FOUND,
                course: null
            };
        }

        const _ = await courseData.getByIds(courses_id, users_id);

        const result = await courseData.unEnrollByTeacher(courses_id, users_id);
       
        return {
            error: null,
            course: result,
        };
    };
};

export default {
    getAllCourses,
    getAllPublicCourses,
    getCourseById,
    createCourse,
    updateCourse,
    updateCourseToPublic,
    updateCourseToPrivate,
    deleteCourse,
    getEnrolledStudentsId,
    getNotEnrolledStudentsId,
    enrollCourse,
    unEnrollCourse,
    enrollCoursebyEmail,
    unEnrollCourseByEmail
};