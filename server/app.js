import express from 'express';
import helmet from 'helmet';
import cors from 'cors';
import passport from 'passport';
import jwtStrategy from './auth/strategy.js';
import usersController from './controllers/users-controller.js';
import authController from './controllers/auth-controller.js';
import coursesController from './controllers/courses-controller.js';
import sectionsController from './controllers/sections-controller.js';

const app = express();

const PORT = 5555;

passport.use(jwtStrategy);

app.use(helmet());
app.use(cors());
app.use(express.json());

app.use(passport.initialize());
app.use('/users', usersController);
app.use('/auth', authController);
app.use('/courses', coursesController);
app.use('/', sectionsController);
app.listen(PORT, () => console.log(`Listening on port ${PORT}`));