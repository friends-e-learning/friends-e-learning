import pool from './pool.js';

const getAll = async () => {
    const sql = `
        SELECT id, title, description, isPublic
        FROM courses
        WHERE isDeleted = 0   
    `;

    return await pool.query(sql);
};

const getAllPublic = async () => {
    const sql = `
        SELECT id, title, description, isPublic
        FROM courses
        WHERE isDeleted = 0
        AND isPublic = 1
    `;

    return await pool.query(sql);
};

const getBy = async (column, value) => {
    const sql = `
    SELECT id, title, description, isPublic
    FROM courses
    WHERE ${column} = ?
    AND isDeleted = 0 
    `;  

    const result = await pool.query(sql, [value]);
   
    return result[0];
};

const searchBy = async (column, value) => {
    const sql = `
        SELECT * 
        FROM courses
        WHERE ${column} LIKE '%${value}%' 
        AND isPublic = 1 AND isDeleted = 0 
    `; 

    return await pool.query(sql);
};

const getCourseByTitle = async (title) => {
    const res = await pool.query('select * from courses where courses.title = ? AND isPublic = 0 AND isDeleted = 0 ', [title]);
  
    if (res[0]) {
        return res[0];
    }
  
    return null;
};

const create = async (title, description, isPublic) => {
    const sql = `
      INSERT into courses (title, description, isPublic)
      VALUES (?, ?, ?)
    `;
  
    const result = await pool.query(sql, [title, description, isPublic]);

    return {
        course_id: result.insertId,
        title: title,
        description: description,
        isPublic: isPublic
    };
};

const update = async (course) => {
    const { id, title, description } = course;
    const sql = `
        UPDATE courses SET
          title = ?,
          description = ?
        WHERE id = ?
        AND isDeleted = 0
    `;

    return await pool.query(sql, [title, description, id]);
};

const setPublic = async (course) => {
    const sql = `
        UPDATE courses 
        SET isPublic = 1
        WHERE id = ?
        AND isDeleted = 0
    `;

    return await pool.query(sql, [course.id]);
};
const setPrivate = async (course) => {
    const sql = `
        UPDATE courses 
        SET isPublic = 0
        WHERE id = ?
        AND isDeleted = 0
    `;

    return await pool.query(sql, [course.id]);
};

const remove = async (course) => {
    const sql = `
        UPDATE courses 
        SET isDeleted = 1
        WHERE id = ?
        AND isDeleted = 0
    `;

    return await pool.query(sql, [course.id]);
};

const getByIds = async (courses_id, users_id) => {
    const sql = `
        SELECT * 
        FROM student_courses
        WHERE courses_id = ? AND users_id = ?
    `;
    const enroll = await pool.query(sql, [courses_id, users_id]);

    return enroll;
};

const enroll = async (courses_id, users_id) => {
    const sql = `
        INSERT into student_courses (courses_id, users_id)
        VALUES (?, ?)
    `;
  
    await pool.query(sql, [courses_id, users_id]);
    const courseEnroll = getByIds(courses_id, users_id);

    return courseEnroll;
       
};
const postenrollByEmail = async (courses_id, users_id) => {
    const sql = `
        INSERT into student_courses (courses_id, users_id)
        VALUES (?, (SELECT id FROM users WHERE email = ?))
    `;
  
    await pool.query(sql, [courses_id, users_id]);
    const courseEnroll = getByIds(courses_id, users_id);

    return courseEnroll;
       
};

const unEnroll = async (courses_id, users_id) => {
    const sql = `
        DELETE FROM student_courses
        WHERE courses_id = ? AND users_id = ?
    `;
  
    await pool.query(sql, [courses_id, users_id]);
    const courseUnEnroll = getByIds(courses_id, users_id);

    return courseUnEnroll;
       
};

const unEnrollByTeacher = async (courses_id, email) => {
    const sql = `
        DELETE FROM student_courses
        WHERE courses_id = ? and users_id=(SELECT id FROM users WHERE email = ?)
  `;

    await pool.query(sql, [courses_id, email]);
    const courseUnEnroll = getByIds(courses_id, email);

    return courseUnEnroll;
       
};

const getEnrolledStudents = async (courses_id) => {
    const sql = `
        select u.email, u.firstName, u.lastName, r.name as role
        from student_courses sc
        join users u on u.id=sc.users_id
        join roles r on r.id=u.roles_id
        where sc.courses_id = ?
    `;

    const result = await pool.query(sql, [courses_id]);

    return result;

};

const getNotEnrolledStudents = async (courses_id) => {
    const sql = `
        SELECT u.id, u.email, u.firstName, u.lastName, r.name as role
        from users u
        join roles r on r.id=u.roles_id
        LEFT JOIN student_courses sc on u.id = sc.users_id
        WHERE (u.id IS NULL OR (u.id NOT IN (SELECT sc.users_id FROM student_courses sc WHERE sc.courses_id = ?))) 
        AND u.isDeleted = 0 
        GROUP BY u.id;
    `;

    const result = await pool.query(sql, [courses_id]);

    return result;

};

export default {
    getAll,
    getAllPublic,
    getBy,
    searchBy,
    getCourseByTitle,
    create,
    update,
    setPublic,
    setPrivate,
    remove,
    getByIds,
    enroll,
    unEnroll,
    getEnrolledStudents,
    getNotEnrolledStudents,
    postenrollByEmail,
    unEnrollByTeacher
};