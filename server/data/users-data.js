import pool from './pool.js';

const getAll = async () => {
    const sql = `
        SELECT id, email, firstName, lastName, createdOn  
        FROM users
        WHERE isDeleted = 0
    `; 

    return await pool.query(sql);
};

const getWithRole = async (email) => {
    const sql = `
        SELECT u.id, u.email, u.firstName, u.lastName, u.password, u.createdOn, r.name as role
        FROM users u
        JOIN roles r ON u.roles_id = r.id
        WHERE u.email = ? 
        AND isDeleted = 0
    `;

    const result = await pool.query(sql, [email]);

    return result[0];
};

const getBy = async (column, value) => {
    const sql = `
        SELECT id, email, firstName, lastName
        FROM users
        WHERE ${column} = ?
        AND isDeleted = 0
    `;

    const result = await pool.query(sql, [value]);

    return result[0];
};

const searchBy = async (column, value) => {
    const sql = `
        SELECT id, email, firstName, lastName 
        FROM users
        WHERE ${column} LIKE '%${value}%'
        AND isDeleted = 0
    `;

    return await pool.query(sql);
};

const create = async (email, firstName, lastName, password, role) => {
    const sql = `
        INSERT INTO users(email, firstName, lastName, password, roles_id)
        VALUES (?,?,?,?,(SELECT id FROM roles WHERE name = ?))
    `;

    const result = await pool.query(sql, [email, firstName, lastName, password, role]);

    return {
        id: result.insertId,
        email: email,
        firstName: firstName,
        lastName: lastName,
    };
};

const update = async (user) => {
    const { id, email, password, firstName, lastName } = user;
    const sql = `
        UPDATE users SET
          email = ?,
          password = ?,
          firstName = ?,
          lastName = ?
        WHERE id = ?
        AND isDeleted = 0
    `;

    return await pool.query(sql, [email, password, firstName, lastName, id]);
};

const remove = async (user) => {
    const sql = `
        UPDATE users 
        SET isDeleted = 1
        WHERE id = ?
        AND isDeleted = 0
    `;

    return await pool.query(sql, [user.id]);
};

const getStudentsEnrolledCourses = async (id) => {
    const sql = `
        select u.email, u.firstName, u.lastName, c.title, c.id as courses_id, c.description
        from student_courses sc
        join users u on u.id=sc.users_id
        join courses c on c.id=sc.courses_id
        where sc.users_id = ${id} 
        AND c.isDeleted = 0
        AND u.isDeleted = 0
    `;

    const result = await pool.query(sql);

    return result;

};

export default {
    getAll,
    getWithRole,
    searchBy,
    getBy,
    create,
    update,
    remove,
    getStudentsEnrolledCourses
};
