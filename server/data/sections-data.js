import pool from './pool.js';

const getSections = async (id) => {
    const sql = `
        SELECT s.id, s.title, s.content, s.order_by, s.availableFrom, c.id as courseId, c.title  as courseTitle
        FROM sections s
        JOIN courses c ON c.id = s.courses_id
        WHERE courses_id = ${id} AND s.isDeleted = 0
        order by s.order_by ASC
    `;

    const result = await pool.query(sql);

    return result;
};

const getBy = async (column, value) => {
    const sql = `
        SELECT * 
        FROM sections
        WHERE ${column} = ? AND isDeleted = 0
    `;  

    const result = await pool.query(sql, [+value]);
    
    return result[0];
};

const create = async (title, content, order, availableFrom, id) => {
    const sql = `
        INSERT into sections (title, content, order_by, availableFrom, courses_id)
        VALUES (?, ?, ?, ?, ?)
    `;
  
    const result = await pool.query(sql, [title, content, order, availableFrom, id]);
    
    const res1 = getBy('id', result.insertId);
    
    return res1;
};

const update = async (section) => {
    const { id, title, content, order_by, availableFrom } = section;
    const sql = `
        UPDATE sections SET
          title = ?,
          content = ?,
          order_by = ?,
          availableFrom = ?
         
        WHERE id = ?
    `;

    return await pool.query(sql, [title, content, order_by, availableFrom, id]);
};

const remove = async (section) => {
    const sql = `
        UPDATE sections
        SET isDeleted = 1
        WHERE id = ? AND isDeleted = 0
    `;
  
    return await pool.query(sql, [section.id]);
};

const getSectionsOrder = async (id) => {
    const sql = `
        SELECT s.id, s.title, s.order_by, c.id as courseId, c.title as courseTitle
        FROM sections s
        JOIN courses c ON c.id = s.courses_id
        WHERE courses_id = ${id} AND s.isDeleted = 0
        order by s.order_by ASC
    `;

    const result = await pool.query(sql);

    return result;
};
const updateSectionsOrderUp = async (section) => {
    const { id, order_by } = section;
    const sql = `
        UPDATE sections
        SET order_by = order_by + 1 
        WHERE order_by >= ?
    `;

    const sql1 = `
        UPDATE sections 
        SET order_by=?
        WHERE id = ?   
    `
   
    ;
    const result = await pool.query(sql, [order_by]);
    const result2 = await pool.query(sql1, [order_by,id] );
    return [result, result2];

};
const updateSectionsOrderDown = async (section) => {
    const { id, order_by } = section;
    const sql = `
        UPDATE sections
        SET order_by = order_by + 1
        WHERE order_by > ? 
    `; 
    
    const sql1 = `
        UPDATE sections 
        SET order_by=? + 1
        WHERE id = ?  `   
    ;
    const result = await pool.query(sql, [order_by]);
    const result2 = await pool.query(sql1, [order_by,id] );
    return [result, result2];

};

export default {
    getSections,
    getBy,
    create,
    update,
    remove,
    getSectionsOrder,
    updateSectionsOrderUp,
    updateSectionsOrderDown
};

