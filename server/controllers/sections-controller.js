/* eslint-disable no-unused-vars */
import express from 'express';
import serviceError from '../services/service-error.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import sectionsService from '../services/sections-service.js';
import sectionsData from '../data/sections-data.js';
import { createValidator } from '../validations/validation-middleware.js';
import { createSectionSchema } from '../validations/schemas/create-section.js';

const sectionsController = express.Router();

sectionsController
    // get all sections of a single course
    .get('/courses/:id/sections', 
        authMiddleware, 
        async (req, res) => {
            const { id } = req.params;
            const role = req.user.role;
            const user = req.user;

            const { error, sections } = await sectionsService.getAllSections(sectionsData)(+id, role, user);
            
            res.status(200).send(sections);
            
        })
    // get section by id
    .get('/courses/:id/sections/:sectionId',
        authMiddleware,
        async (req, res) => {
            const { sectionId, id } = req.params;
            const role = req.user.role;
            const user = req.user;

            const { error, sections } = await sectionsService.getSectionById(sectionsData)(+sectionId, +id, role, user);
           
            res.status(200).send(sections);
           
        })

    // create new section of a course
    .post('/courses/:id/sections',
        authMiddleware, 
        roleMiddleware('Teacher'),
        createValidator(createSectionSchema), 
        async (req, res) => {
            const { id } = req.params;
            const section = req.body;
            const { error, data } = await sectionsService.createSection(sectionsData)(section, +id);
        
            if (error === serviceError.RECORD_NOT_FOUND){
                res.status(404).send({ message: 'Course does not exist!' });
            } else {
                res.status(200).send(data);
            }  
        })

    // edit section by id
    .put('/courses/:courseId/sections/:sectionId', 
        authMiddleware, 
        // createValidator(createSectionSchema), 
        async (req, res) => {
            const role = req.user.role;
            const sectionId = +req.params.sectionId;
            const courseId = +req.params.courseId;
            const updateData = req.body;

            const { error, section } = await sectionsService.updateSection(sectionsData)(sectionId, updateData, courseId, role);
            if (error === serviceError.RECORD_NOT_FOUND){
                res.status(404).send({ message: 'Course or section not found' });
            } else if (error === serviceError.OPERATION_NOT_PERMITTED){
                res.status(405).send({ message: 'You are not allowed to do such an operation' });
            } else {
                res.status(200).send(section);
            }
        })

    // delete section
    .delete('/courses/:id/sections/:sectionId', 
        authMiddleware, 
        async (req, res) => {
        
            const role = req.user.role;
            const id = +req.params.sectionId;
            const courseId = +req.params.id;
            const { error, comment } = await sectionsService.deleteSection(sectionsData)(id, courseId, role);
    
            if (error === serviceError.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'Section not found!' });
            }
            else if (error === serviceError.OPERATION_NOT_PERMITTED){
                res.status(405).send({ message: 'You are not allowed to do such an operation' });
            }
            else {
                res.status(200).send({ message: 'Section was successfully deleted' });
            }
        })

    // order section up
    .post('/courses/:courseId/sections/:sectionId/up', 
        authMiddleware, 
        async (req, res) => {
            const role = req.user.role;
            const sectionId = +req.params.sectionId;
            const courseId = +req.params.courseId;
            const updateData = req.body;

            const { error, section } = await sectionsService.updateSectionByOrderUp(sectionsData)(sectionId, updateData, courseId, role);
            if (error === serviceError.RECORD_NOT_FOUND){
                res.status(404).send({ message: 'Course or section not found' });
            } else if (error === serviceError.OPERATION_NOT_PERMITTED){
                res.status(405).send({ message: 'You are not allowed to do such an operation' });
            } else {
                res.status(200).send(section);
            }
        })

    // order sections down
    .post('/courses/:courseId/sections/:sectionId/down', 
        authMiddleware, 
        async (req, res) => {
            const role = req.user.role;
            const sectionId = +req.params.sectionId;
            const courseId = +req.params.courseId;
            const updateData = req.body;

            const { error, section } = await sectionsService.updateSectionByOrderDown(sectionsData)(sectionId, updateData, courseId, role);
            if (error === serviceError.RECORD_NOT_FOUND){
                res.status(404).send({ message: 'Course or section not found' });
            } else if (error === serviceError.OPERATION_NOT_PERMITTED){
                res.status(405).send({ message: 'You are not allowed to do such an operation' });
            } else {
                res.status(200).send(section);
            }
        });

export default sectionsController;