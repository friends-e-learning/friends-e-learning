import express from 'express';
import { createCourseSchema } from '../validations/schemas/create-course.js';
import coursesData from '../data/courses-data.js';
import coursesService from '../services/courses-service.js';
import serviceError from '../services/service-error.js';
import { authMiddleware, roleMiddleware } from '../auth/auth-middleware.js';
import { createValidator } from '../validations/validation-middleware.js';

/*eslint no-unused-vars: "off"*/

const coursesController = express.Router();

coursesController
// get all courses with searching
    .get('/', 
        async (req, res) => {
            const { search } = req.query;
            const courses = await coursesService.getAllCourses(coursesData)(search);

            res.status(200).send(courses);
        })

// get all public courses with searching
    .get('/public', 
        async (req, res) => {
            const { search } = req.query;
            const courses = await coursesService.getAllPublicCourses(coursesData)(search);

            res.status(200).send(courses);
        })

// get course by id
    .get('/:id', 
        authMiddleware, 
        async (req, res) => {
            const { id } = req.params;
            const role = req.user.role;
            const user = req.user;
          
            const { error, course } = await coursesService.getCourseById(coursesData)(+id, role, user);
            
            res.status(200).send(course);
          
        })

    // create new course 
    .post('/', 
        authMiddleware, 
        roleMiddleware('Teacher'),
        async (req, res) => {
            const createCourse = req.body;
            const role = req.user.role;
        
            const { course, error } = await coursesService.createCourse(coursesData)(createCourse, role);
            if (error === serviceError.OPERATION_NOT_PERMITTED) {
                res.status(405).send({ message: 'You are not allowed to do such operation' });
            } else if (error === serviceError.DUPLICATE_RECORD) {
                res.status(409).send({ message: 'There is already a course with the same name' });
            }
            return res.status(200).send(course);
        })

    // edit course
    .put('/:id', 
        authMiddleware, 
        roleMiddleware('Teacher'),
        createValidator(createCourseSchema), 
        async (req, res) => {
            const role = req.user.role;
            const updateData = req.body;
            const id = +req.params.id;
           
            const { error, course } = await coursesService.updateCourse(coursesData)(id, updateData, role);
            if(error === serviceError.RECORD_NOT_FOUND){
                res.status(404).send({ message: 'Course not found' });
            } else if(error === serviceError.OPERATION_NOT_PERMITTED){
                res.status(405).send({ message: 'You are not allowed to do such an operation' });
            } else{
                res.status(200).send(course);
            }
        })

    // make course public
    .put('/:id/public', 
        authMiddleware, 
        roleMiddleware('Teacher'),
        async (req, res) => {
            const role = req.user.role;
            const id = +req.params.id;
           
            const { error, course } = await coursesService.updateCourseToPublic(coursesData)(id, role);
            if(error === serviceError.RECORD_NOT_FOUND){
                res.status(404).send({ message: 'Course not found' });
            } else if(error === serviceError.OPERATION_NOT_PERMITTED){
                res.status(405).send({ message: 'You are not allowed to do such an operation' });
            } else{
                res.status(200).send({ message: 'You successfully updated this course to public' });
            }
        })

    // make course private
    .put('/:id/private', 
        authMiddleware, 
        roleMiddleware('Teacher'),
        async (req, res) => {
            const role = req.user.role;
            const id = +req.params.id;
           
            const { error, course } = await coursesService.updateCourseToPrivate(coursesData)(id, role);
            if(error === serviceError.RECORD_NOT_FOUND){
                res.status(404).send({ message: 'Course not found' });
            } else if(error === serviceError.OPERATION_NOT_PERMITTED){
                res.status(405).send({ message: 'You are not allowed to do such an operation' });
            } else{
                res.status(200).send({ message: 'You successfully updated this course to private' });
            }
        })

    // delete course
    .delete('/:id', 
        authMiddleware,
        roleMiddleware('Teacher'),
        async (req, res) => {
            const { id } = req.params;
            const role = req.user.role;
           
            const { error, course } = await coursesService.deleteCourse(coursesData)(+id, role);
        
            if (error === serviceError.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'Post not found!' });
            } else if (error === serviceError.OPERATION_NOT_PERMITTED) {
                res.status(403).send({ message: 'You are not allowed to do such an operation' });        
            } else {
                res.status(200).send({ message: 'Post was successfully deleted' });
            }
        })

    // enroll in course by teacher
    .post('/:id/enroll', 
        authMiddleware, 
        roleMiddleware('Teacher'),

        async (req, res) => {
            const enrolledStudent = req.body.email;
            const courseId = req.params.id;

            const { error, course } = await coursesService.enrollCoursebyEmail(coursesData)(+courseId, enrolledStudent);
            if(error === serviceError.RECORD_NOT_FOUND){
                res.status(404).send({ message: 'Course not found' });
            } else{
                res.status(200).send({ message: `${enrolledStudent} was successfully enrolled in the course` });
            }
        })

    // enroll in course by student
    .put('/:id/enroll', 
        authMiddleware, 
        
        async (req, res) => {
            const userId = req.user.id;
            const courseId = req.params.id;

            const { error, course } = await coursesService.enrollCourse(coursesData)(courseId, userId);
            if(error === serviceError.RECORD_NOT_FOUND){
                res.status(404).send({ message: 'Course not found' });
            } else{
                res.status(200).send(course);
            }
        })

    // unenroll from course by student
    .delete('/:id/enroll', 
        authMiddleware, 
    
        async (req, res) => {
            const userId = req.user.id;
            const courseId = req.params.id;

            const { error, course } = await coursesService.unEnrollCourse(coursesData)(courseId, userId);
            if(error === serviceError.RECORD_NOT_FOUND){
                res.status(404).send({ message: 'Course not found' });
            } else{
                res.status(200).send(course);
            }
        })

    // unenroll from course by teacher
    .delete('/:id/unenroll', 
        authMiddleware, 
        roleMiddleware('Teacher'),

        async (req, res) => {
            const enrolledStudent = req.body.email;
            const courseId = req.params.id;

            const { error, course } = await coursesService.unEnrollCourseByEmail(coursesData)(+courseId, enrolledStudent);
            if(error === serviceError.RECORD_NOT_FOUND){
                res.status(404).send({ message: 'Course not found' });
            } else{
                res.status(200).send({ message: `${enrolledStudent} was successfully unenrolled from the course` });
            }
        })

    // get all enrolled students in a course
    .get('/:id/enrolled/:usersId', 
        authMiddleware,
        async (req, res) => {
            const { courses_id, id } = req.params;

            const { error, data } = await coursesService.getEnrolledStudentsId(coursesData)(+courses_id, +id);

            if (error === serviceError.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'Course not found!' });
            } else {
                res.status(200).send(data);
            }
        })

    // get all not enrolled students in a course
    .get('/:id/unenrolled/:usersId', 
        authMiddleware,
        async (req, res) => {
            const { courses_id, id } = req.params;

            const { error, data } = await coursesService.getNotEnrolledStudentsId(coursesData)(+courses_id, +id);

            if (error === serviceError.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'Course not found!' });
            } else {
                res.status(200).send(data);
            }
        });

export default coursesController;