import express from 'express';
import createToken from '../auth/create-token.js';
import usersService from '../services/user-service.js';
import usersData from '../data/users-data.js';
import serviceErrors from '../services/service-error.js';

const authController = express.Router();

authController
    // sign-in 
    .post('/signin', async (req, res) => {
        const { email, password } = req.body;
        const { error, user } = await usersService.signInUser(usersData)(email, password);

        if (error === serviceErrors.INVALID_SIGNIN) {
            res.status(400).send({
                message: 'Invalid username or password'
            });
        } 

        else {
            const payload = {
                sub: user.id,
                email: user.email,
                role: user.role,
                firstName: user.firstName,
                lastName: user.lastName,
            };
            const token = createToken(payload);

            res.status(200).send({
                token: token
            });
        }
    });

export default authController;