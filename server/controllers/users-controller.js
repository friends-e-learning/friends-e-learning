/* eslint-disable no-unused-vars */
import express from 'express';
import usersService from '../services/user-service.js';
import usersData from '../data/users-data.js';
import serviceError from '../services/service-error.js';
import { createValidator } from '../validations/validation-middleware.js';
import { createUserSchema } from '../validations/schemas/create-user.js';
import { updateUserSchema } from '../validations/schemas/update-user.js';
import { authMiddleware } from '../auth/auth-middleware.js';
import bcrypt from 'bcrypt';

const usersController = express.Router();

usersController
    // get all users with searching
    .get('/', 
        authMiddleware, 
        async (req, res) => {
            const { search } = req.query;
            const users = await usersService.getAllUsers(usersData)(search);

            res.status(200).send(users);
        })

    // get user by id
    .get('/:id',
        authMiddleware,
        async (req, res) => {
            const { id } = req.params;
            const { error, user } = await usersService.getUserById(usersData)(+id);

            if (error === serviceError.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'User not found!' });
            } else {
                res.status(200).send(user);
            }
        })

    // get student's courses 
    .get('/:id/enrolled', 
        authMiddleware, 
        async (req, res) => {
            const { id } = req.params;
            const { error, courses } = await usersService.getEnrolledCoursesId(usersData)(+id);

            if(error === serviceError.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'User not found!' });
            } else {
                res.status(200).send(courses);
            }
        })

    // create user
    .post('/',
        createValidator(createUserSchema),
        async (req, res,) => {
            const createData = req.body;

            const { error, user } = await usersService.createUser(usersData)(createData);
            if (error === serviceError.DUPLICATE_RECORD) {
                res.status(409).send({ message: 'There is already a user with the same e-mail. Please, select new one' });
            } else {
                res.status(201).send(user);
            }
        })
        
    // update user by id
    .put('/:id',
        authMiddleware, 
        createValidator(updateUserSchema),
        async (req, res) => {
            const { id } = req.params;
            const userId = req.user.id;
            const role = req.user.role;
            const updateData = req.body;

            const passwordHash = await bcrypt.hash(updateData.password, 10);
            updateData.password = passwordHash;

            const { error, user } = await usersService.updateUser(usersData)(+id, userId, updateData, role);

            if (error === serviceError.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'User not found!' });
            } else if(error === serviceError.OPERATION_NOT_PERMITTED) {
                res.status(403).send({ message: 'You are allowed to change only your own profile!' });
            } else {
                res.status(200).send(user);
            }
        })

    // delete user by id
    .delete('/:id',
        authMiddleware, 
        async (req, res) => {
            const { id } = req.params; 
            const userId = req.user.id;
            const role = req.user.role;
            const { error, user } = await usersService.deleteUser(usersData)(+id, userId, role);

            if (error === serviceError.RECORD_NOT_FOUND) {
                res.status(404).send({ message: 'User not found!' });
            }else if(error === serviceError.OPERATION_NOT_PERMITTED) {
                res.status(403).send({ message: 'You are allowed to delete only your own profile!' });
            }
            else {
                res.status(200).send({ message: 'User deleted' });
            }
        });

export default usersController;
