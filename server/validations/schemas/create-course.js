export const createCourseSchema = {
    title: value => {
        if (!value) {
            return 'Title is required';
        }
        
        if (typeof value !== 'string' || value.length < 5 || value.length > 50) {
            return 'Title should be a string in range [5..50]';
        }

        return null;
    },
    description: value => {
        if (!value) {
            return 'Description is required';
        }
        
        if (typeof value !== 'string' || value.length < 10 || value.length > 100) {
            return 'Description should be a string in range [10..100]';
        }

        return null;
    }
};