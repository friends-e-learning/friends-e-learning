export const createSectionSchema = {
    title: value => {
        if (!value) {
            return 'Title is required';
        }
        
        if (typeof value !== 'string' || value.length < 5 || value.length > 1000) {
            return 'Title should be a string in range [5..1000]';
        }

        return null;
    },
    content: value => {
        if (!value) {
            return 'Content is required';
        }
        
        if (typeof value !== 'string' || value.length < 10 || value.length > 1000) {
            return 'Content should be a string in range [10..1000]';
        }

        return null;
    }
};