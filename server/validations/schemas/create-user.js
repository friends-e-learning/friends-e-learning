export const createUserSchema = {
    email: value => {
        if (!value) {
            return 'E-mail is required';
        }
        
        // eslint-disable-next-line no-useless-escape
        const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
        if (typeof value !== 'string' || value.length < 10 || value.length > 25 || value.search(emailRegex)) {
            return 'Invalid E-mail or should be a string in range [10..25]';
        }

        return null;
    },
    firstName: value => {
        if (!value) {
            return 'First name is required';
        }
        
        if (typeof value !== 'string' || value.length < 2 || value.length > 15) {
            return 'First name should be a string in range [2..15]';
        }

        return null;
    },
    lastName: value => {
        if (!value) {
            return 'Last name is required';
        }
        
        if (typeof value !== 'string' || value.length < 1 || value.length > 15) {
            return 'Last name should be a string in range [1..15]';
        }

        return null;
    },
    password: value => {
        if (!value) {
            return 'Password is required';
        }
        
        if (typeof value !== 'string' || value.length < 6 || value.length > 15) {
            return 'Password should be a string in range [6..15]';
        }

        return null;
    },
};