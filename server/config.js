export const DB_CONFIG = {
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: '1511',
    database: 'friends-e-learning'
};

export const PRIVATE_KEY = 'very_secret_key';

export const TOKEN_LIFETIME = 365 * 60 * 60; 

export const DEFAULT_USER_ROLE = 'Student';