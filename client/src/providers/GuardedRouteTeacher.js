import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from  'prop-types';

const GuardedRouteTeacher = ({ component: Component, auth, teacher,...rest }) => (
   
   
  <Route
    {...rest} render={(props) => (
        auth === true && teacher === true
            ? <Component {...props} />
            : <Redirect to='/courses' />
    )}
  />
);

GuardedRouteTeacher.propTypes = {
  component: PropTypes.func.isRequired,
  auth: PropTypes.bool.isRequired,
  teacher: PropTypes.bool.isRequired
};

export default GuardedRouteTeacher;