import jwtDecode from "jwt-decode";
import { createContext } from "react";

export const getToken = () => localStorage.getItem('token') || '';

export const extractUser = token => {
  try {
    return jwtDecode(token);
  } catch {
    return null;
  }
}

const AuthContext = createContext({
    isLoggedIn: false,
    user: null,
    setUser: () => {},
});

export default AuthContext;

