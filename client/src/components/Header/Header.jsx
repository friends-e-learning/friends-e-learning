import React, { useContext } from 'react';
import { Button } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import roles from '../../common/roles.enum';
import AuthContext from '../../providers/AuthContext';
import './Header.css';
import Logo from './logo.jpg';

/*eslint no-unused-vars: "off"*/

const Header = () => {

    const { isLoggedIn, setLoginState } = useContext(AuthContext);
    const { user } = useContext(AuthContext);
    const history = useHistory();
    
    const triggerLogout = () => {
        
        localStorage.removeItem('token');
        history.push('/');
        setLoginState({
            isLoggedIn: false,
            user: null,
          })
    }

    return (
        <header className="header">
        <img className="logo" src={Logo} alt="Logo" />&nbsp;&nbsp;&nbsp;&nbsp;<span className="red-dot"></span>&nbsp;
        
        {!user
        ? <div className="header-links"><Link to='/courses/public' className="link">Our courses</Link>&nbsp;<span className="blue-dot"></span>&nbsp;</div>
        : null
        }

        {user
        ? <div className="header-links"><Link to='/courses' className="link">Our courses</Link>&nbsp;<span className="blue-dot"></span>&nbsp;</div>
        : null
        }

        {user && user.role === roles.TEACHER
        ? <div className="header-links"><Link to='/courses/new' className="link">Create course</Link>&nbsp;<span className="yellow-dot"></span>&nbsp;</div>
        : null
        }

        { isLoggedIn && user.role === roles.STUDENT
        ? <div className="header-links"><Link to={`/users/${user.sub}/courses`} className="link" key={user.sub}>My courses</Link>&nbsp;<span className="yellow-dot"></span>&nbsp;</div>
        : null
        }

        { isLoggedIn && user.role === roles.STUDENT
        ? <div className="header-links"><Link to={`/users/${user.sub}`} className="link" key={user.sub}>My profile</Link>&nbsp;<span className="yellow-dot"></span>&nbsp;</div>
        : null
        }

        { isLoggedIn && user.role === roles.TEACHER
        ? <div className="header-links"><Link to='/users' className="link">Our users</Link>&nbsp;<span className="red-dot"></span>&nbsp;</div>
        : null
        }
        
        { isLoggedIn 
        ? `How you doin', ${user.firstName}?`
        : null
        }

        { isLoggedIn 
        ? <div className="header-links"><Button className="button" variant="dark" onClick={triggerLogout}>Logout</Button> </div>
        : <div className="header-links"><Link to='/' className="link">Sign in</Link></div>
        } &nbsp;<span className="yellow-dot"></span>&nbsp;
            
        </header>
    )
};

export default Header;