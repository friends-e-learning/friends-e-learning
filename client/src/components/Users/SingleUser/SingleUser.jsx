import { Box, Typography } from '@material-ui/core';
import { Rating } from '@material-ui/lab';
import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { Card } from 'react-bootstrap';
import ratings from '../../../common/rating.enum';
import { getStudentsCoursesRequest } from '../../../requests/user-requests';
import './SingleUser.css';

/*eslint no-unused-vars: "off"*/

const SingleUser = ({ user, id }) => {

    const [value, setValue] = useState(null);
    const [studentCourses, setStudentCourses] = useState([]);
    const [error, setError] = useState(null);
    const [rating, setRating] = useState('');

    useEffect(() => {
        getStudentsCoursesRequest(id)
            .then(courses => setStudentCourses(courses))
            .then( rating => {
                if (studentCourses.length < 3) {
                setValue(1);
                setRating(ratings.BEGINNER)
            } else if (studentCourses.length >= 3 && studentCourses.length <= 5) {
                setValue(2);
                setRating(ratings.POODLE)
            } else if (studentCourses.length >= 6 && studentCourses.length <= 9) {
                setValue(3);
                setRating(ratings.PIVOT)
            } else if (studentCourses.length >= 10 && studentCourses.length <= 12) {
                setValue(4);
                setRating(ratings.ENTHUSIAST)
            } else {
                setValue(5);
                setRating(ratings.BEST_FRIEND)
            } 
        })
            .catch(error => setError(error.message));
    }, [id, studentCourses.length]);

    return (
        <div className="user">
        <div className='card-user'>
            <Card style={{ width: '36rem' }}>
                <Card.Body>
                    <Card.Title><h1>{user.email}</h1></Card.Title>
                        <h2>{user.firstName} {user.lastName}</h2>
                        <div>
                            <Box component="fieldset" mb={3} borderColor="transparent">
                                <Typography component="legend">{rating}</Typography>
                                <Rating name="read-only" value={value} readOnly />
                            </Box>
                        </div>
                    <Card.Link href={`/users/${user.id}/courses`}>{user.firstName}'s courses</Card.Link>
                </Card.Body>
            </Card>
        </div>
        </div>
    );
};

export default SingleUser;