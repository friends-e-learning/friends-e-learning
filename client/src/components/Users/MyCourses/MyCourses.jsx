import { useState, useEffect, useContext } from "react"
import { Button, Card } from "react-bootstrap";
import { Link } from "react-router-dom";
import AuthContext from "../../../providers/AuthContext";
import { getAllCourses } from "../../../requests/course-requests";
import { getStudentsCoursesRequest, getUserById } from "../../../requests/user-requests";
import AppError from "../../Error/AppError";
import fountain1 from './fountain1.jpg';

/*eslint no-unused-vars: "off"*/


const MyCourses = (props) => {
    const id = props.match.params.id;
    const [myCourses, setMyCourses] = useState([]);
    const [error, setError] = useState(null);
    const { user } = useContext(AuthContext);

    const coursesCount = myCourses.length;
    
    useEffect(() => {
        getStudentsCoursesRequest(id)
            .then(courses => setMyCourses(courses))
            .catch(error => setError(error.message));
    }, [id]);

    if (error) {
        return (
          <AppError message="error"/>
        );
    }

    return (
        <div className="container" >
            <h1>OH... <span className="red-dot"></span> MY... <span className="blue-dot"></span> GOD! <span className="yellow-dot"></span><br /> </h1>
            {myCourses.length > 0
                ? <div>{myCourses.length === 1

                    ? <h1>You're <span className="red-dot"></span> enrolled <span className="blue-dot"></span> in {coursesCount} <span className="yellow-dot"></span> course</h1>
                    : <h1>You're <span className="red-dot"></span> enrolled <span className="blue-dot"></span> in {coursesCount} <span className="yellow-dot"></span> courses</h1>
                }
                </div>
                : <h1>You're <span className="red-dot"></span> not enrolled <span className="blue-dot"></span> in a course <span className="yellow-dot"></span> yet</h1>
            }
            <div className="row">
                {myCourses.map(myCourses => <div className="col-md-4 col-sm-6" key={myCourses.courses_id}>
                    <Link to={`/courses/${myCourses.courses_id}`}><Card>
                        <Card.Img variant="top" src={fountain1} />
                        <Card.Body>
                            <Card.Text>
                                {myCourses.title}<br />
                                {myCourses.description}
                            </Card.Text>
                        </Card.Body>
                    </Card>
                    </Link>

                </div>)}
            </div>
        </div>
    )
}

export default MyCourses;