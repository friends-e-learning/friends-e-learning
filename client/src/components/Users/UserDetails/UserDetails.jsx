import React, { useEffect, useState } from "react";
import { useContext } from "react";
import AuthContext from "../../../providers/AuthContext";
import SingleUser from "../SingleUser/SingleUser";
import { Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { deleteUserRequest, editUserRequest, getUserById } from "../../../requests/user-requests";
import roles from "../../../common/roles.enum";
import { Modal } from "react-bootstrap";
import credentialsLength from "../../../common/credentials-length.enum";
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { IconButton, Tooltip } from "@material-ui/core";
 /*eslint no-unused-vars: "off"*/

const UserDetails = (props) => {

    const id = props.match.params.id;

    const [userDetails, setUserDetails] = useState({});
    const [updateUser, setUpdateUser] = useState({
        email: '',
        password: '',
        firstName: '',
        lastName: ''
    });
    const [deleteUser, setDeleteUser] = useState(false);
    const [error, setError] = useState(null);
    const history = useHistory();

    const [isOpen, setIsOpen] = useState(false);

    const showModal = () => {
      setIsOpen(true);
    };
  
    const hideModal = () => {
      setIsOpen(false);
    };

    const [show, setShow] = useState(false);

    const handleClose = () => {
        setShow(false);
    };
    const handleShow = () => {
        setShow(true);
    };    

    const { user, setUser } = useContext(AuthContext);
    useEffect(() => {
        getUserById(id)
        .then(user => setUserDetails(user))
        .catch (error => setError(error.message));
    }, [id]);
    
    useEffect(() => {
        getUserById(id)
        .then(user => setUpdateUser(user))
        .catch (error => setError(error.message));
    }, [id]);

    const updatedUser = (id, email, firstName, lastName, password) => {

        if (!updateUser.password
            || updateUser.password.length < credentialsLength.passwordMinLength 
            || updateUser.password.length > credentialsLength.passwordMaxLength) {
            alert('Password must be at least 6 and max 15 symbols')
        }
        else if (updateUser.firstName.length < credentialsLength.firstNameMinLength 
            || updateUser.firstName.length > credentialsLength.firstNameMaxLength) {
            alert('First name must be at least 2 and max 15 symbols')
        }
       else if (updateUser.lastName.length < credentialsLength.lastNameMinLength 
            || updateUser.lastName.length > credentialsLength.lastNameMaxLength) {
            alert('Last name must be at least 1 and max 15 symbols')
        }

        
        editUserRequest(id, email, firstName, lastName, password)
        .then(data => {
            if(data.message) {
                return alert(data.message)
            }
        })
        .then(history.goBack)
        .catch (error => setError(error.message));
    }

    const deleteSingleUser = () => {
        deleteUserRequest(id)
        .then(deleteUser => setDeleteUser(true))
        .then(alert('You successfully deleted this user'))
        .then(() => props.history.push('/users'))
        .catch (error => setError(error.message));
    }

    return (
        <div className="container userDetails">
        <SingleUser key={id} user={userDetails} id={id}/>
        {user.sub === userDetails.id
        ? <><Tooltip title="Edit profile">
            <EditIcon style={{width: 35, height: 45, cursor: 'default'}} onClick={showModal}></EditIcon>
        </Tooltip>
            <Modal animation={false} show={isOpen} onHide={hideModal}>
            <Modal.Header>
                <Modal.Title>You can edit you profile here</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            <div className="edit-user-form">
                <h3 className="edit-user">
                    Edit your profile
                </h3>
                <form>
                    <div>
                        <h6>E-mail</h6>
                        <input className="user-area" value={updateUser.email} disabled/>
                    </div><br/>
                    <div>
                        <h6>Password</h6>
                        <input className="user-area" type="password" onChange={(e) => {updateUser.password = e.target.value; setUpdateUser({...updateUser})}}/>
                    </div>
                    <div>
                        <h6>First name</h6>
                        <input className="user-area" value={updateUser.firstName} onChange={(e) => {updateUser.firstName = e.target.value; setUpdateUser({...updateUser})}} />
                    </div><br/>
                    <div>
                        <h6>Last name</h6>
                        <input className="user-area" value={updateUser.lastName} onChange={(e) => {updateUser.lastName = e.target.value; setUpdateUser({...updateUser})}} />
                    </div><br/>
                    

                </form>
            </div>
            </Modal.Body>
            <Modal.Footer>
                <button onClick={hideModal}>Cancel</button>
                <button className="button" onClick={e => updatedUser(id, updateUser)}>Save</button >
            </Modal.Footer>
            </Modal>
        </>
        : null
        }

            {user.role === roles.TEACHER
            ?  
            <>&nbsp;
            <Tooltip title="Delete profile">
            <DeleteIcon style={{fontSize: "xx-large"}} onClick={handleShow}></DeleteIcon>
            </Tooltip>
                <Modal animation={false} show={show} onHide={handleClose}>
                <Modal.Header>
                    <Modal.Title>Delete profile</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you sure you want to delete this profile?</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                    Close
                    </Button>
                    <Button className="button" variant="light" onClick={() => deleteSingleUser(deleteUser)}>Delete user</Button>
                </Modal.Footer>
                </Modal>
            </>                   
            : null 
            }
        </div>
    )
};

export default UserDetails;