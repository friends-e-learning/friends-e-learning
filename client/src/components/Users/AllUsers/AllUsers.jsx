import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { getAllUsers } from '../../../requests/user-requests';
import AppError from '../../Error/AppError';
import './AllUsers.css'
import umbrella from './umbrella.jpeg';
/*eslint no-unused-vars: "off"*/

const AllUsers = () => {
    const [users, setUsers] = useState([])
    const [keyWord, setKeyWord] = useState('');
    const [error, setError] = useState(null);

    useEffect(() =>{
        getAllUsers()
        .then(data => setUsers(data))
        .catch (error => setError(error.message));
    }, []);

    if (error) {
        return (
          <AppError message="error"/>
        );
    }

    return (
        
        <div className="container users">
       
            <div className="text-users">
            <h1 className="allUsersTitle">Our lobsters</h1>
            <h4 className="search-users">Search</h4>
            <span>
                <input className='search-input-users' type="text" value={keyWord} onChange={e => setKeyWord(e.target.value)}></input>
            </span>
            <span>
                <button className="button" variant="outline-light" onClick={() => setKeyWord('')}>Clear</button>
            </span>
            
            <div className="allUsers">
            {
                users
                    .filter(u => keyWord === '' || u.firstName.toLowerCase().includes(keyWord.toLowerCase()) || u.lastName.toLowerCase().includes(keyWord.toLowerCase()))
                    .map(u => <Link to={`./users/${u.id}`} key={u.id}>{u.firstName} {u.lastName}<br/></Link>)
            }
            </div>
            </div>
            
        <img className="umbrellas" src={umbrella} alt="umbrellas" />
        </div>
    );
};

export default AllUsers;