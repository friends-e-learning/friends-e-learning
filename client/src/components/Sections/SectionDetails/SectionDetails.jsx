import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { deleteSection, getSectionsById } from "../../../requests/sections-requests";
import SingleSection from "../SingleSection/SingleSection";
import { Button, Modal } from "react-bootstrap";
import AppError from "../../Error/AppError";
import './SectionDetails.css';
import AuthContext from "../../../providers/AuthContext";
import { useContext } from "react";

/*eslint no-unused-vars: "off"*/

const SectionDetails = (props) => {
  const { courseId, sectionId } = props.match.params;
  const [sectionDetails, setSectionDetails] = useState({});
  const [deleteSingleSection, setDeleteSingleSection] = useState(false);
  const [error, setError] = useState(null);
  const history = useHistory();
  const { user } = useContext(AuthContext)

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const deletedSection = (courseId, sectionId, data) => {
    let mounted = true;
    deleteSection(courseId, sectionId, data)
      .then(result =>{
        if(mounted){
          setDeleteSingleSection(true);
        } 
      }) 
      .then(history.goBack())
      .catch(error => setError(error.message));
      return mounted = false;
  }


  useEffect(() => {
    let mounted = true;
    getSectionsById(courseId, sectionId)
      .then(result =>
        setSectionDetails(result))
      .catch(error => setError(error.message));
  }, [courseId, sectionId])

  if (error) {
    if (user.role === "Teacher") {
      return (
        <AppError message="No such section!" />
      );
    } else if (user.role === "Student") {
      return (
        <AppError message="You are not authorized!" />
      );

    }
    return (
      <AppError message="error" />
    );
  }

  return (
    <div className='container section'>
      <div className="section-details" section={sectionDetails}>
        <SingleSection section={SectionDetails} />
        <h1><div dangerouslySetInnerHTML={{ __html: sectionDetails.title }} /></h1>
        <h2><div dangerouslySetInnerHTML={{ __html: sectionDetails.content }} /></h2>
        {user.role === "Teacher"
          ? <>
            <Link to={`/courses/${courseId}/sections/${sectionId}/edit`}>

              <button className="button">Edit section</button>{' '}

            </Link>
            <>
              <Button variant="primary" onClick={handleShow}>
                Delete section
              </Button>

              <Modal animation={false} show={show} onHide={handleClose}>
                <Modal.Header>
                  <Modal.Title>Delete section</Modal.Title>
                </Modal.Header>
                <Modal.Body>Are you sure you want to delete this section?</Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={handleClose}>
                    Close
                  </Button>
                  <Button variant="primary" onClick={(e) => { handleClose(); deletedSection(courseId, sectionId, deleteSingleSection) }}>
                    Delete
                  </Button>
                </Modal.Footer>
              </Modal>
            </>
          </>
          : null
        }
      </div>
    </div>
  )
};

export default SectionDetails;