import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { ListGroup } from 'react-bootstrap';
import { getCourseSections, updateSectionOrderUp, updateSectionOrderDown } from '../../../requests/sections-requests';
import SingleSection from '../SingleSection/SingleSection';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import { Draggable } from 'react-beautiful-dnd';
import AppError from '../../Error/AppError';
import './OrderSections.css';

/*eslint no-unused-vars: "off"*/

const OrderSections = (props) => {

    const id = props.match.params.id;
    const sectionId = props.match.params.sectionId;
    const [sections, setSections] = useState([]);
    const [order, setOrder] = useState([]);
    const [error, setError] = useState(null);
    const [updateSection, setUpdateSection] = useState({
        order_by: ''
    });

      useEffect(() => {
        getCourseSections(id)
            .then(data => setSections(data))
            .catch(error => setError(error.message));
    }, [id]);

    
    const updateOrderUp = (id, sectionId, data) => {
        updateSectionOrderUp(id, sectionId, data)
        .then(result => {
            if (result.error) {
                throw new Error(result.message);
            }
            setUpdateSection(updateSection);
              })
        .catch(error => setError(error.message));
    }

    const updateOrderDown = (id, sectionId, data) => {
        updateSectionOrderDown(id, sectionId, data)
        .then(result => {
            if (result.error) {
                throw new Error(result.message);
            }
            setUpdateSection(updateSection);
             })
        .catch(error => setError(error.message));
    }

    if (error) {
        return (
          <AppError message="error"/>
        );
    }

    return (
        <div className="container order-sections">

            <DragDropContext
                onDragEnd={(param) => {
                    const srcIndex = param.source.index;
                    const destIndex = param.destination?.index;
                    const sectionId = +param.draggableId;
                    const orderUp = sections[destIndex].order_by;
                    const orderDown = sections[destIndex].order_by;
                   
                    if (order || destIndex || destIndex === 0 || srcIndex === 0) {
                        sections.splice(destIndex, 0, sections.splice(srcIndex, 1)[0]);
                        if (srcIndex > destIndex){
                            updateOrderUp(id, +sectionId, {order_by: orderUp}) 
                        } 
                        else {
                            updateOrderDown(id, +sectionId, {order_by: orderDown}) 

                        }
                        
                     }
                }}
            > 
                <ListGroup className="list-group">
                    <h1>Order sections</h1>
                    <Droppable droppableId="droppable-1">
                        {(provided, _) => (
                            <div ref={provided.innerRef} {...provided.droppableProps}>

                                {sections.map((s, i) => (
                                    <Draggable 
                                        key={s.id}
                                        draggableId={`${s.id}`}
                                        index={i}>
                                        {(provided, _) => (
                                            <ListGroup.Item ref={provided.innerRef} {...provided.draggableProps} {...provided.dragHandleProps} >
                                                <div >
                                                    
                                                    <SingleSection key={s.id} {...s} section={s}/>
                                                    
                                                </div>
                                            </ListGroup.Item>
                                        )}

                                    </Draggable>))}

                                {provided.placeholder}
                            </div>
                        )}
                    </Droppable>
                </ListGroup>
            </DragDropContext>
        </div>
    )
};

export default OrderSections;