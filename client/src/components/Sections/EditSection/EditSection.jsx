import { Button } from 'bootstrap';
import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { getSectionsById, updateSection as updateSectionRequest } from '../../../requests/sections-requests';
import moment from 'moment';
import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-html";
import "ace-builds/src-noconflict/theme-monokai";
import "ace-builds/src-noconflict/ext-language_tools";
import "ace-builds/src-noconflict/ext-beautify";
import { beautify } from "ace-builds/src-noconflict/ext-beautify";
import AppError from '../../Error/AppError';
import './EditSection.css';
import door from './door.png';

/*eslint no-unused-vars: "off"*/

const EditSection = (props) => {

    const { courseId, sectionId } = props.match.params;
    const [updateSection, setUpdateSection] = useState({
        title: '',
        content: '',
        order_by: '',
        availableFrom: ''
    });

    const [error, setError] = useState(null);
    const history = useHistory();

    useEffect(() => {
        getSectionsById(courseId, sectionId)
        .then(data => setUpdateSection({...data, availableFrom: moment(data.availableFrom).format('YYYY-MM-DD')}))
        .catch(error => setError(error.message));
    }, [courseId, sectionId])

    const updatedSection = (courseId, sectionId, data) => {
        let mounted = true;
        updateSectionRequest(courseId, sectionId, data)
        .then(result => {
            if (result.error) {
                throw new Error(result.message);
            }else if(mounted){
                setUpdateSection(result);
              }   
        })
        .then(history.push(`/courses/${courseId}`))
        .catch(error => setError(error.message));
        return mounted = false;
    }

    if (error) {
        return (
          <AppError message="error"/>
        );
    }

    return(
        <div className="edit-section">
            <img src={door} className="door" alt='door'/>
            <div className="edit-section-form">
            <h1>
                Edit Section
            </h1>
            <br/>
            <form>

            <input maxLength={1000} className="edit-section-input" value={updateSection.title} onChange={(e) => {updateSection.title = e.target.value; setUpdateSection({...updateSection})}} /><br/>
            <br/> 
            {updateSection.title.length < 5
                ? <p className="length-warning">Title must be at least 5 symbols</p>   
                :null
            }

            <div className="ace-editor">
            <AceEditor
                className="editor"
                style={{ width: '600px', height: '200px', border: 'solid black 2 px' }}
                placeholder='Your content...'
                value={updateSection.content}
                commands={beautify.commands}
                mode='html'
                theme='textmate'
                fontSize={16}
                showPrintMargin={true}
                showGutter={true}
                highlightActiveLine={true}
                onChange={(e) => setUpdateSection({...updateSection, content: e})}
                setOptions={{
                    enableBasicAutocompletion: true,
                    enableLiveAutocompletion: true,
                    enableSnippets: true,
                    showLineNumbers: true,
                    tabSize: 4,
                    wrap: 1
                }}

            />
            <br/>
            </div>
                <h3 className="create-section-order">Order:</h3>
                <input maxLength={3} className="text-area-order" value={updateSection.order_by || ''} onChange={(e) => {updateSection.order_by = e.target.value; setUpdateSection({...updateSection})}} /><br/>
                
                {updateSection.order_by.length < 1 || updateSection.order_by.length > 3
                ? <p className="length-warning">Order must be at least 1 and max 3 symbols</p>
                : null
                }

            <div>
                <p>Available from</p>
                <input type="date" value={moment(updateSection.availableFrom).format('YYYY-MM-DD')} onChange={(e) => {updateSection.availableFrom = e.target.value; setUpdateSection({...updateSection})}}/>
            </div>

            {updateSection.title.length < 5 || updateSection.content.length < 10 || updateSection.order_by.length < 3
            ? null
            :   < button className="button" variant="info" onClick={e => updatedSection(courseId, sectionId, updateSection)}>Update section</button>
            }
               
            </form>
            </div>
        </div>
    )
};

export default EditSection;