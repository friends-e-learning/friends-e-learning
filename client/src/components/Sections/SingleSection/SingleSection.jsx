import React from "react";
import { useState } from "react";
import { useContext } from "react";
import { Link } from "react-router-dom";
import AuthContext from "../../../providers/AuthContext";
import { getSectionsById } from "../../../requests/sections-requests";
import { updateSection as updateSectionRequest } from "../../../requests/sections-requests";
import roles from "../../../common/roles.enum";
import AppError from "../../Error/AppError";
import { convertFromUTCTime } from "../../../common/dates";

/*eslint no-unused-vars: "off"*/

const SingleSection = (props) => {

    const courseId = props.courseId;
    const sectionId = props.id;
    const [sectionDetails, setSectionDetails] = useState({});
    const [error, setError] = useState(null);
    const [updateSection, setUpdateSection] = useState({
        order_by: ''
    });
    const {user} = useContext(AuthContext);

    if (error) {
        return (
          <AppError message="error"/>
        );
    }
    
    return (
        <>{user?.role === roles.STUDENT &&  Date.now() < new Date(props.availableFrom).getTime()
        ? 
        <div className="container" section={sectionDetails}>
            <div id="title">            
                <div dangerouslySetInnerHTML={{ __html: props.title}}/>
                <p>(Section will be available from {convertFromUTCTime(props.availableFrom)})</p>
            </div>  

        </div>
        : <div className="container" section={sectionDetails}>
            <div id="title"> <Link to={`/courses/${courseId}/sections/${sectionId}`} ><div dangerouslySetInnerHTML={{ __html: props.title}}/></Link></div>
        </div>
        }
        </>
    )
};

export default SingleSection;