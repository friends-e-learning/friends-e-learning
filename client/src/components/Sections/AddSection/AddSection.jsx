import React, { useState } from "react";
import { useHistory } from "react-router";
import { Button } from "react-bootstrap";
import { createSection, getCourseSections } from "../../../requests/sections-requests";
import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-html";
import "ace-builds/src-noconflict/theme-monokai";
import "ace-builds/src-noconflict/ext-language_tools";
import "ace-builds/src-noconflict/ext-beautify";
import { beautify } from "ace-builds/src-noconflict/ext-beautify";
import "ace-builds/webpack-resolver";
import AppError from "../../Error/AppError";
import { useEffect } from "react";
import door from './door.png';
import './AddSection.css';

/*eslint no-unused-vars: "off"*/

const CreateSectionForm = ({ match }) => {

    const id = match.params.id;
    const [sections, setSections] = useState([]);
    const [section, setSection] = useState({
        title: '',
        content: '',
        order: '',
        availableFrom: ''
    });
    const [error, setError] = useState(null);
    const history = useHistory();

    useEffect(() => {
        getCourseSections(id)
            .then(data => setSections(data))
            .catch(error => setError(error.message));
    }, [id]);

    const submitSection = (id, section) => {
        let mounted = true;
        createSection(id, section)
            .then(result => {
                if (result.error) {
                    throw new Error(result.message);
                }else if(mounted){
                    setSection(result);
                  }  
                
            })
            .then(history.goBack())
            .catch(error => setError(error.message));
            return mounted = false;
    }

    if (error) {
        return (
            <AppError message="error" />
        );
    }

    return (
        <div className="addSection">
            <img src={door} className="door" alt='door'/>
            <div className="new-section">
                <h3 className="add-section-title">
                    Create Section
                </h3>
                <form>
                    <div>
                        <input maxLength={1000} className="add-section-input" value={section.title} onChange={(e) => { section.title = e.target.value; setSection({ ...section }) }} /><br />
                    </div>

                    {section.title.length < 5
                        ? <p className="length-warning">Title must be at least 5 symbols</p>
                        : null
                    }

                    <div className="ace-editor">
                        <AceEditor
                            className="editor"
                            style={{ width: '800px', height: '200px', border: 'solid black 2 px' }}
                            placeholder='Your content...'
                            value={section.content}
                            commands={beautify.commands}
                            mode='html'
                            theme='textmate'
                            fontSize={16}
                            showPrintMargin={true}
                            showGutter={true}
                            highlightActiveLine={true}
                            onChange={(e) => setSection({ ...section, content: e })}
                            setOptions={{
                                enableBasicAutocompletion: true,
                                enableLiveAutocompletion: true,
                                enableSnippets: true,
                                showLineNumbers: true,
                                tabSize: 4,
                                wrap: 1,
                            }}

                        />
                    </div>
                    <div>
                        <p>Available from</p>
                        <input type="date" value={section.availableFrom} onChange={(e) => { section.availableFrom = e.target.value; setSection({ ...section }) }} />
                    </div>

                    {section.title.length < 5
                        ? null
                        : sections.length > 0
                         ?
                         < Button className="button" variant="primary" onClick={e => submitSection(id, { ...section, order: sections[sections.length - 1].order_by + 1 })}>Create section</Button>
                         :
                         < Button className="button" variant="primary" onClick={e => submitSection(id, { ...section, order: 1 })}>Create section</Button>                    
                    }
                </form>
            </div>
        </div>
    )
}

export default CreateSectionForm;