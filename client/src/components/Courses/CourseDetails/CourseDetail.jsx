import React, { useEffect, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { Button, Modal } from "react-bootstrap";
import { useContext } from "react";
import AuthContext from "../../../providers/AuthContext";
import { deleteCourse, getCourseById, privateCourseRequest, publicCourseRequest } from "../../../requests/course-requests";
import { getCourseSections } from "../../../requests/sections-requests";
import SingleSection from "../../Sections/SingleSection/SingleSection";
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import { getEnrolledStudents, unenrollRequest } from "../../../requests/enroll-requests";
import roles from "../../../common/roles.enum";
import './CourseDetail.css';
import Unagi1 from './Unagi1.jpg'
import AppError from "../../Error/AppError";

/*eslint no-unused-vars: "off"*/

const CourseDetails = (props) => {

  const id = props.match.params.id;
  const courses_id = props.match.params.id
  const [courseDetails, setCourseDetails] = useState({});
  const [sections, setSections] = useState([]);
  const [delCourse, setDeleteCourse] = useState(false);
  const [error, setError] = useState(null);
  const { user } = useContext(AuthContext);
  const [publicCourse, setPublicCourse] = useState(false);
  const [privateCourse, setPrivateCourse] = useState(false);
  const [unenroll, setUnenroll] = useState([]);
  const [email, setEmail] = useState([]);
  const history = useHistory();


  const [state, setState] = useState({
    checkedA: false,
    checkedB: true,
  });

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);


  useEffect(() => {
    getCourseById(id)
      .then(course => setCourseDetails(course))

      .catch(error => setError(error.message));
  }, [id]);

  useEffect(() => {
    getCourseSections(id)
      .then(data => setSections(data))
      .catch(error => setError(error.message));
  }, [id]);

  const deleteSingleCourse = (id, data) => {
    deleteCourse(id, data)
      .then(data => setDeleteCourse(data))
      .then(() => props.history.push('/courses'))

      .catch(error => setError(error.message));
  };

  const makeCoursePublic = (id) => {
    publicCourseRequest(id)
      .then(data => {
        setState(state);
        setPublicCourse(true);

        getCourseById(id)
          .then(course => setCourseDetails(course))

      })
      .catch(error => setError(error.message));
  };

  const makeCoursePrivate = (id) => {
    privateCourseRequest(id)
      .then(data => {

        setPrivateCourse(true);
        setState(state);
        getCourseById(id)
          .then(course => setCourseDetails(course))
      })
      .catch(error => setError(error.message));
  }

  const unEnrollFromPublicCourse = (id) => {
    unenrollRequest(id)
      .then(data => {
        setUnenroll(data);
        getEnrolledStudents(id, courses_id)
          .then(data => setEmail(data))
        getCourseById(id)
          .then(course => setCourseDetails(course))
          .then(history.goBack())
          .catch(error => setError(error.message));
      })
      .catch(error => setError(error.message));
  };

  if (error) {
    if (user.role === roles.TEACHER && !courseDetails.id) {
      return (
        <AppError message="No such course!" />
      );
    }
    else if (user.role === "Student") {
      return (
        <AppError message="You are not authorized!" />
      );
    }
    else {
      return (
        <AppError message="Error" />
      )
    }
  }

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };


  return (
    <div className="course-details" course={courseDetails}>

      <div className="course-details-header">
        <img className="unagi" src={Unagi1} alt="unagi" />
        <h1>{courseDetails.title}</h1>
      </div><br />

      <div className="allSections">
        {
          sections.map(s => <SingleSection key={s.id} {...s} section={s} />)
        }
      </div>

      {user.role === roles.TEACHER && courseDetails.isPublic === 0
        ? <>
          <Link to={`/courses/${id}/enroll`}>
            <Button className="button" variant="primary">Students</Button>
          </Link>
        </>
        : null}
      {user.role === roles.STUDENT
        ? <Button className="button" variant="light" onClick={(e) => unEnrollFromPublicCourse(id, unenroll)}>Unenroll from this course</Button>
        : null
      }

      {user.role === roles.TEACHER
        ? <>
          <Link to={`/courses/${id}/edit`}>
            <Button className="button" variant="light">Edit course</Button>{' '}
          </Link>
          <Link to={`/courses/${id}/new`}>
            <Button className="button" variant="light">Add new section</Button>{' '}
          </Link>
          <Link to={`/courses/${id}/sections/order`}>
            <Button className="button" variant="light">Order sections</Button>{' '}
          </Link>

          <>
            <Button variant="primary" onClick={handleShow}>
              Delete course
            </Button>

            <Modal animation={false} show={show} onHide={handleClose}>
              <Modal.Header>
                <Modal.Title>Delete course</Modal.Title>
              </Modal.Header>
              <Modal.Body>Are you sure you want to delete this course?</Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                  Close
                </Button>
                <Button variant="primary" onClick={(e) => { handleClose(); deleteSingleCourse(id, delCourse) }}>
                  Save Changes
                </Button>
              </Modal.Footer>
            </Modal>
          </>
        </>
        : null
      }

      {user.role === roles.TEACHER
        ? <div className="switch-button"> {courseDetails.isPublic === 1
          ? <FormGroup row>

            <FormControlLabel
              control={
                <Switch
                  checked={state.checkedA}
                  onChange={(e) => { handleChange(e); makeCoursePrivate(id) }}
                  name="checkedA"
                  color="primary"
                />
              }
              label="Make private"
            />
          </FormGroup>
          : <FormGroup row>

            <FormControlLabel
              control={
                <Switch
                  checked={state.checkedB}
                  onChange={(e) => { handleChange(e); makeCoursePublic(id) }}
                  name="checkedB"
                  color="primary"
                />
              }
              label="Make public"
            />
          </FormGroup>
        } </div>
        : null
      }
    </div>
  );

}

export default CourseDetails;