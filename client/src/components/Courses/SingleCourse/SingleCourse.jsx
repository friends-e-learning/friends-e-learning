import React, { useState } from "react";
import { useEffect } from "react";
import { useContext } from "react";
import { Link } from "react-router-dom";
import roles from "../../../common/roles.enum";
import AuthContext from "../../../providers/AuthContext";
import { getCourseById } from "../../../requests/course-requests";
import { enrollRequest, getEnrolledStudents } from "../../../requests/enroll-requests";
import { makeStyles } from '@material-ui/core/styles';
import { Card } from "react-bootstrap";
import fountain1 from './fountain1.jpg';
import AppError from "../../Error/AppError";
import { Box } from "@material-ui/core";
import { Rating } from "@material-ui/lab";

/*eslint no-unused-vars: "off"*/

const SingleCourse = ({ course }) => {

    const id = course.id;
    const courses_id = course.id
    const [enroll, setEnroll] = useState([]);
    const [error, setError] = useState(null);
    const [courseDetails, setCourseDetails] = useState({});
    const [value, setValue] = useState(null);
    const { user } = useContext(AuthContext);

    const enrollEmail = enroll.map(e => e.email);
    useEffect(() => {
        getEnrolledStudents(courses_id, id)
            .then(data => setEnroll(data))
            .then(rating => {
                if(enroll.length <= 5) {
                    setValue(1);
                } else if (enroll.length <= 10) {
                    setValue(2);
                } else if(enroll.length <= 15) {
                    setValue(3);
                } else if(enroll.length <= 20) {
                    setValue(4);
                } else if(enroll.length <= 25) {
                    setValue(5);
                }
            })
            .catch(error => setError(error.message));
    }, [id, courses_id, enroll.length]);

    const enrollInPublicCourse = (id) => {
        enrollRequest(id)
            .then(data => {
                setEnroll(data);
                getEnrolledStudents(courses_id, id)
                    .then(data => setEnroll(data))
                    .catch(error => setError(error.message));
            })
            .catch(error => setError(error.message));
    };

    useEffect(() => {
        getCourseById(id)
            .then(course => setCourseDetails(course))

            .catch(error => setError(error.message));
    }, [id]);

    const useStyles = makeStyles({
        root: {
            maxWidth: 345,
        },
        media: {
            height: 140,
        },
    });

    const classes = useStyles();

    if (error) {
        return (
          <AppError message="error"/>
        );
    }

    return (
        <div className="singleCourse" course={courseDetails}>

            {!(enrollEmail.includes(user.email)) && user.role === roles.STUDENT
                ?
                <>
                <Link to={`/courses/${course.id}`} onClick={(e) => enrollInPublicCourse(id, enroll)}>
                <Card>
                    <Card.Img variant="top" src={fountain1} />
                    <Card.Body>
                        {course.title}
                        {course.description}
                        <div>
                            <Box component="fieldset" mb={3} borderColor="transparent">
                                <Rating name="read-only" value={value} readOnly />
                            </Box>
                        </div>
                    </Card.Body>
                </Card>
                </Link>
                <br />
                </>
                : <>
                <Link to={`/courses/${course.id}`}><Card>
                    <Card.Img variant="top" src={fountain1} />
                    <Card.Body>
                        {/* <Card.Text> */}
                        {course.title}<br/>
                        {course.description}
                        <div>
                            <Box component="fieldset" mb={3} borderColor="transparent">
                                <Rating name="read-only" value={value} readOnly />
                            </Box>
                        </div>
                        {/* </Card.Text> */}
                    </Card.Body>
                </Card>
                </Link>
                <br />
                </>
            }

        </div>
    )
}

export default SingleCourse;