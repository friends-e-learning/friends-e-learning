import { useContext } from "react";
import { useState, useEffect } from "react"
import { Button } from "react-bootstrap";
import roles from "../../../common/roles.enum";
import AuthContext from "../../../providers/AuthContext";
import { getAllCourses, getAllPublicCourses } from "../../../requests/course-requests";
import AppError from "../../Error/AppError";
import SingleCourse from "../SingleCourse/SingleCourse";
import './AllCourses.css';

/*eslint no-unused-vars: "off"*/


const AllCourses = () => {
    const [courses, setCourses] = useState([]);
    const [publicCourses, setPublicCourses] = useState([]);
    const [keyWord, setKeyWord] = useState('');
    const [error, setError] = useState(null);
    const {user} = useContext(AuthContext);

    useEffect(() =>{
       getAllCourses()
        .then(data => setCourses(data))
        .catch (error => setError(error.message));
    },[])

    useEffect(() =>{
        getAllPublicCourses()
         .then(data => setPublicCourses(data))
         .catch (error => setError(error.message));
     },[])

    if (error) {
        return (
          <AppError message="error"/>
        );
    }
 
    return (
        <div className="container">
            <div className="text-courses">
                <h1>Joey&nbsp;<span className="red-dot"></span>&nbsp;doesn't&nbsp;<span className="blue-dot"></span>&nbsp;share&nbsp;<span className="yellow-dot"></span>&nbsp;food...</h1>
                    <h1>But&nbsp;<span className="red-dot"></span>&nbsp;we&nbsp;<span className="blue-dot"></span>&nbsp;share&nbsp;<span className="yellow-dot"></span>&nbsp;our&nbsp;<span className="red-dot"></span>&nbsp;knowledge&nbsp;<span className="blue-dot"></span>&nbsp;with&nbsp;<span className="yellow-dot"></span>&nbsp;you</h1>
            <h1 className="allCoursesTitle">Subscribe&nbsp;<span className="red-dot"></span>&nbsp;in&nbsp;<span className="blue-dot"></span>&nbsp;our&nbsp;<span className="yellow-dot"></span>&nbsp;courses&nbsp;</h1><br/>
            <span className="search">Search</span>
            <span>
                <input className='search-input' type="text" value={keyWord} onChange={e => setKeyWord(e.target.value)}></input>
            </span>
            <span>
                <button className="button" onClick={() => setKeyWord('')}>Clear</button>
            </span>

            </div>
            <div className="row">
                
            { user.role === roles.TEACHER
                ? courses
                    .filter(c => keyWord === '' || c.title.toLowerCase().includes(keyWord.toLowerCase()) || c.description.toLowerCase().includes(keyWord.toLowerCase()))
                    .map(c => <div className="col-md-4 col-sm-6" key={c.id}><SingleCourse course={c}/></div>).reverse()
                : publicCourses
                    .filter(c => keyWord === '' || c.title.toLowerCase().includes(keyWord.toLowerCase()) || c.description.toLowerCase().includes(keyWord.toLowerCase()))
                    .map(c => <div className="col-md-4 col-sm-6" key={c.id}><SingleCourse course={c}/></div>).reverse()
            }
            </div>
        </div>

    )
}

export default AllCourses