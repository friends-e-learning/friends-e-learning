import React, { useState } from "react";
import { useHistory } from "react-router";
import { Button } from "react-bootstrap";
import { createCourseRequest } from "../../../requests/course-requests";
import { FormControlLabel, FormGroup, Switch } from "@material-ui/core";
import './CreateCourse.css'
import AppError from "../../Error/AppError";
/*eslint no-unused-vars: "off"*/

const CreateCourse = () => {
    const [course, setCourse] = useState({
        title: '',
        description: '',
        isPublic: 0
    });

    const [state, setState] = useState({
        checkedA: false,
        checkedB: true,
    });

    const [error, setError] = useState(null);

    const history = useHistory()

    const submitCourse = (data) => {
        let mounted = true;
        createCourseRequest(data)
        .then(data => {
            if (data.message) {
               return alert(data.message)
            }
        })
            .then(newCourse => {
                if(mounted){
                    setCourse(newCourse);
                  }     
            })
            .catch(error => setError(error.message));
            return mounted = false;
    }

    if (error) {
        return (
            <AppError message="error" />
        );
    }

    const handleChange = (event) => {
        setState({ ...state, [event.target.name]: event.target.checked });
    };

    return (
        <div className="createCourse">

            <div className="create-course-form">
                <h2 className="create-new-course">
                    Create new course
                </h2>
                <form> <h3>Title:</h3>
                    <div className="title">
                        <input maxLength={50} className="text-area-title" value={course.title || ''} onChange={(e) => { course.title = e.target.value; setCourse({ ...course }) }} /><br />
                    </div>

                    {course.title.length < 5 || course.title.length === 50
                        ? <><p className="length-warning">Title must be at least 5 and max 50 symbols</p><br /></>
                        : null
                    }<br/>

                    <h3 className="create-course-description">Description:</h3><br /><br />
                        <div className="description">
                            <textarea maxLength={100} className="text-area-description" value={course.description || ''} onChange={(e) => { course.description = e.target.value; setCourse({ ...course }) }} /><br />
                            
                        </div>

                    {course.description.length < 10 || course.description.length === 100
                        ? <p className="length-warning">Description must be at least 10 and max 100 symbols</p>
                        : null
                    }

                    <div className="switch-button">
                        <FormGroup row>

                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={state.checkedA}
                                        onChange={handleChange}
                                        name="checkedA"
                                        color="primary"
                                    />
                                }
                                label="Private course"
                            />
                        </FormGroup>
                    </div>

                    {state.checkedB
                        ? course.isPublic === 0
                        : course.isPublic === 1
                    }

                    {course.title.length < 5 || course.description.length < 10 || course.description.length === 100 || course.title.length === 50
                        ? null
                        : <Button variant="primary" onClick={e => { submitCourse(course); history.push('/courses') }}>Create course</Button>}

                </form>
            </div>
        </div>
    )
};
export default CreateCourse;