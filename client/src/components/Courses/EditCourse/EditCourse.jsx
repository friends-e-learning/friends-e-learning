import React, {useState} from "react";
import { useEffect } from "react";
import { Button } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { getCourseById, updateCourse } from "../../../requests/course-requests";
import AppError from "../../Error/AppError";
import './EditCourse.css';

const EditCourse = ({match}) => {
    const {id} = match.params;
    const history = useHistory()
    
    const [updatedCourse, setUpdateCourse] = useState({
        title: '',
        description: '',
    });
    const [error, setError] = useState(null)

    useEffect(() => {
        getCourseById(id)
        .then(data => setUpdateCourse(data))
        .catch (error => setError(error.message));
    },[id])

    const editedCourse = (id, data) => {
        updateCourse(id, data)
        .then(data => data)
        .then(history.goBack())
        .catch (error => setError(error.message));
    }
    
    if (error) {
        return (
          <AppError message="error"/>
        );
    }

    return(
        <div className="editCourse">
            <div className="edit-course-form">
            <h1 className="edit-course">
                Edit course
            </h1>
            <form>
                <h3>Title:</h3>
                <div className="title">
                    <input maxLength={45} className="text-area-title"  value={updatedCourse.title} onChange={(e) => {updatedCourse.title = e.target.value; setUpdateCourse({...updatedCourse})}} /><br/>
                    <br/>
                </div>

                {updatedCourse.title.length < 5
                ? <p className="length-warning">Title must be at least 3 symbols</p>
                : null}

                <h3>Description:</h3>
                <div className="text">
                    <textarea maxLength={10000} className="text-area-description" value={updatedCourse.description} onChange={(e) => {updatedCourse.description = e.target.value; setUpdateCourse({...updatedCourse})}} /><br/><br/>
                    
                </div>
                
                {updatedCourse.description.length < 10
                ? <p className="length-warning">Description must be at least 10 symbols</p>
                : null
                } <br/>

            {updatedCourse.title.length < 5 || updatedCourse.description.length < 10  
               ? null
               : <Button className="button" variant="light" onClick={e => editedCourse(id,updatedCourse)}>Update Course</Button >
            }          
            </form>
            </div>
        </div>
    )
}

export default EditCourse