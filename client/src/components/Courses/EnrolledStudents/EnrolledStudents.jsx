import React from 'react';
import { useEffect } from 'react';
import { useContext } from 'react';
import { useState } from 'react';
import { Table } from 'react-bootstrap';
import roles from '../../../common/roles.enum';
import AuthContext from '../../../providers/AuthContext';
import { getCourseById } from '../../../requests/course-requests';
import { enrollTeacherRequest, getEnrolledStudents, getNotEnrolledStudents, unEnrollTeacherRequest } from '../../../requests/enroll-requests';
import AppError from '../../Error/AppError';
import './EnrolledStudents.css';

/*eslint no-unused-vars: "off"*/

const EnrolledStudents = (props) => {

  const id = props.match.params.id;
  const courses_id = props.match.params.id;

  const [email, setEmail] = useState([]);
  const [notEnrolled, setNotEnrolled] = useState([]);
  const [enroll, setEnroll] = useState([]);
  const [error, setError] = useState(null);
  const [courseDetails, setCourseDetails] = useState({});
  const { user } = useContext(AuthContext);
  const [keyWordEnrolled, setKeyWordEnrolled] = useState('');
  const [keyWordNotEnrolled, setKeyWordNotEnrolled] = useState('');

  useEffect(() => {
    getCourseById(id)
      .then(course => setCourseDetails(course))

      .catch(error => setError(error.message));
  }, [id]);

  useEffect(() => {
    getEnrolledStudents(courses_id, id)
      .then(data => setEnroll(data))
      .catch(error => setError(error.message));
  }, [id, courses_id]);

  useEffect(() => {
    getNotEnrolledStudents(courses_id, id)
      .then(data => setNotEnrolled(data))
      .catch(error => setError(error.message));
  }, [id, courses_id]);


  const enrollStudentByTeacher = (courses_id, data) => {
    enrollTeacherRequest(courses_id, data)
      .then(data => {
        getEnrolledStudents(courses_id, id)
          .then(data => setEnroll(data))
        getNotEnrolledStudents(courses_id, id)
          .then(data => setNotEnrolled(data))
          .catch(error => setError(error.message));
      })
      .catch(error => setError(error.message));
  }


  const unEnrollStudentByTeacher = (courses_id, data) => {
    unEnrollTeacherRequest(courses_id, data)
      .then(data => {
        getNotEnrolledStudents(courses_id, id)
          .then(data => setNotEnrolled(data))
        getEnrolledStudents(courses_id, id)
          .then(data => setEnroll(data))
          .catch(error => setError(error.message));
      })
      .catch(error => setError(error.message));
  }

  if (error) {
    return (
      <AppError message="error"/>
    );
  }

  return (

    <>{user.role === roles.TEACHER
      ? <div className="tables-students" course={courseDetails} >
      
        <div className="enrolled-students">
        <h1>In this course: {enroll.length} students</h1>
        <input className='search-input-users' type="text" value={keyWordEnrolled} onChange={e => setKeyWordEnrolled(e.target.value)}></input>
          <br/><br/>
          <Table striped bordered hover >
            <thead>
              <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {enroll
              .filter(u => u.role !== roles.TEACHER)
              .filter(u => keyWordEnrolled === '' || u.firstName.toLowerCase().includes(keyWordEnrolled.toLowerCase()) || u.lastName.toLowerCase().includes(keyWordEnrolled.toLowerCase()))
              .map(u => (
                <tr key={u.email} {...u}>
                  <td>{u.firstName}</td>
                  <td>{u.lastName}</td>
                  <td>{u.email}</td>
                  <td><button className="btn btn-primary" onClick={(e) => unEnrollStudentByTeacher(id, { email: u.email })}>Unenroll</button></td>
                </tr>
              ))}
            </tbody>
          </Table>
          </div>
          
          <div className="not-enrolled-students">
          <h1>Not in this course: {notEnrolled.length} students</h1>
          <input className='search-input-users' type="text" value={keyWordNotEnrolled} onChange={e => setKeyWordNotEnrolled(e.target.value)}></input>
          <br/><br/>
          <Table striped bordered hover >
            <thead>
              <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {notEnrolled
              .filter(u => u.role !== roles.TEACHER)
              .filter(u => keyWordNotEnrolled === '' || u.firstName.toLowerCase().includes(keyWordNotEnrolled.toLowerCase()) || u.lastName.toLowerCase().includes(keyWordNotEnrolled.toLowerCase()))
              .map(u => (
                <tr key={u.email} {...u}>
                  <td>{u.firstName}</td>
                  <td>{u.lastName}</td>
                  <td>{u.email}</td>
                  <td><button className="btn btn-primary" onClick={(e) => enrollStudentByTeacher(id, { email: u.email })}>Ennroll</button></td>
                
                </tr>
              ))}
            </tbody>
          </Table>
          </div>
        <br />
        <div>
        </div></div>
      : null
    }
    </>
  )
};

export default EnrolledStudents;