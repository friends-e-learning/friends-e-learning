import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { Card } from 'react-bootstrap';
import { getAllPublicCourses } from '../../../requests/course-requests';
import AppError from '../../Error/AppError';
import fountain1 from './fountain1.jpg';

/*eslint no-unused-vars: "off"*/

const PublicCourses = () => {

    const [publicCourses, setPublicCourses] = useState([]);
    const [error, setError] = useState(null);

    useEffect(() => {
        getAllPublicCourses()
            .then(data => setPublicCourses(data))
            .catch(error => setError(error.message));
    }, [])

    if (error) {
        return (
            <AppError message="error" />
        );
    }

    return (
        <div className="container">
            <div className="text-courses">
                <h1>Joey&nbsp;<span className="red-dot"></span>&nbsp;doesn't&nbsp;<span className="blue-dot"></span>&nbsp;share&nbsp;<span className="yellow-dot"></span>&nbsp;food...</h1>
                <h1>But&nbsp;<span className="red-dot"></span>&nbsp;we&nbsp;<span className="blue-dot"></span>&nbsp;share&nbsp;<span className="yellow-dot"></span>&nbsp;our&nbsp;<span className="red-dot"></span>&nbsp;knowledge&nbsp;<span className="blue-dot"></span>&nbsp;with&nbsp;<span className="yellow-dot"></span>&nbsp;you</h1>
                <h1>Sign-in&nbsp;<span className="red-dot"></span>&nbsp;or&nbsp;<span className="blue-dot"></span>&nbsp;register&nbsp;<span className="yellow-dot"></span>&nbsp;to&nbsp;<span className="red-dot"></span></h1>
                <h1>Subscribe&nbsp;<span className="red-dot"></span>&nbsp;in&nbsp;<span className="blue-dot"></span>&nbsp;our&nbsp;<span className="yellow-dot"></span>&nbsp;courses&nbsp;</h1><br />
            </div>
            <div className="row">
            {publicCourses
                .map(c => <div className="col-md-4 col-sm-6" key={c.id}>
                    <Card>
                        <Card.Img variant="top" src={fountain1} />
                        <Card.Body>
                            <Card.Text>
                                {c.title}
                            </Card.Text>
                        </Card.Body>
                    </Card><br/>
                </div>).reverse()}
                </div>
        </div>
    )
}

export default PublicCourses;