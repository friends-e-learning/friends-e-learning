/* eslint-disable no-useless-escape */
import React from 'react';
import { useState } from 'react';
import { useContext } from 'react';
import { Link, useHistory } from 'react-router-dom';
import AuthContext, { extractUser } from '../../providers/AuthContext';
import { signInUser } from '../../requests/auth-requests';
import AppError from '../Error/AppError';
import umbrellas_couch from './umbrellas_couch.png';
import './Home.css';

/*eslint no-unused-vars: "off"*/

const Home = () => {
    const { setLoginState } = useContext(AuthContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(null);
    const history = useHistory();

    const triggerLogin = () => {
        const emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
        if (!email) {
            return alert('Please, enter e-mail');

        }
        if (email.search(emailRegex)) {
            return alert('Please, enter a correct e-mail');
           
        }
        if (!password) {
            return alert('Please, enter password');
        };

        signInUser(email, password)
            .then(data => {
                if (data.message) {
                    (history.push('/courses'));
                    return alert(data.message)
                }

                localStorage.setItem('token', data.token);
                setLoginState({
                    isLoggedIn: !!extractUser(data.token),
                    user: extractUser(data.token)
                })
            })
            .then(history.push('/courses'))
            .catch(error => setError(error.message))
    };

    if (error) {
        return (
          <AppError message="error"/>
        );
    }

    return (
        <div className='home'>
        <form className="login">
            <div className="login-text">
                <h1>Login</h1>
                <h4>Select e-mail</h4>
                <input type="e-mail" placeholder="Enter e-mail" onChange={e => setEmail(e.target.value)} value={email}></input>
                <br/><br/>
                <h4>Select password</h4>
                <input type="password" placeholder="Enter password" onChange={e => setPassword(e.target.value)} value={password}></input>
                <br /><br />
                <button className="button" variant="outline-light" type="submit" onClick={triggerLogin}>
                    Login
                </button> <br />
                <h3>Still don't have an account?</h3>
                <h3>Please, register here!</h3><Link to="/register"><button className="button">Register</button></Link>
            </div>
        </form>
        <br/>
        <img className="umbrellas_couch" src={umbrellas_couch} alt="umbrellas" />
        </div>
    );
};

export default Home;