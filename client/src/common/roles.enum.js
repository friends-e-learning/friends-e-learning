const roles = {
    TEACHER: 'Teacher',
    STUDENT: 'Student'
}

export default roles;