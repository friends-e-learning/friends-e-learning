const ratings = {
    BEGINNER: 'Beginner',
    POODLE: 'Poodle',
    PIVOT: 'Pivot',
    ENTHUSIAST: 'Enthusiast',
    BEST_FRIEND: 'Best friend'
}

export default ratings;