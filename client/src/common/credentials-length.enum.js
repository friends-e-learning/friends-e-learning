const credentialsLength = {
    emailMinLength: 10,
    emailMaxLength: 25,
    passwordMinLength: 6,
    passwordMaxLength: 15,
    firstNameMinLength: 2,
    firstNameMaxLength: 15,
    lastNameMinLength: 1,
    lastNameMaxLength: 15
}

export default credentialsLength;