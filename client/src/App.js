import './App.css';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Header from './components/Header/Header';
import Home from './components/Home/Home';
import AuthContext, { extractUser, getToken } from './providers/AuthContext';
import { useState } from 'react';
import Register from './components/Register/Register';
import AllUsers from './components/Users/AllUsers/AllUsers';
import GuardedRoute from './providers/GuardedRoute';
import CreateCourse from './components/Courses/CreateCourse/CreateCourse';
import CourseDetails from './components/Courses/CourseDetails/CourseDetail';
import AllCourses from './components/Courses/AllCourses/AllCourses';
import UserDetails from './components/Users/UserDetails/UserDetails';
import 'bootstrap/dist/css/bootstrap.min.css';
import EditCourse from './components/Courses/EditCourse/EditCourse';
import SectionDetails from './components/Sections/SectionDetails/SectionDetails';
import CreateSectionForm from './components/Sections/AddSection/AddSection';
import EditSection from './components/Sections/EditSection/EditSection';
import MyCourses from './components/Users/MyCourses/MyCourses';
import EnrolledStudents from './components/Courses/EnrolledStudents/EnrolledStudents';
import OrderSections from './components/Sections/OrderSections/OrderSections';
import PublicCourses from './components/Courses/PublicCourses/PublicCourses';
import GuardedRouteTeacher from './providers/GuardedRouteTeacher';

function App() {

  const [user, setUser] = useState({
    isLoggedIn: !!extractUser(getToken()),
    user: extractUser(getToken())
  });
 
  const isTeacher = user?.user?.role === "Teacher";

  return (
    <BrowserRouter>
        <AuthContext.Provider value = {{...user, setLoginState: setUser}}>
      <Header/>
      <Switch>
      <Route exact path='/' component={Home}/>
      <Route exact path='/register' component={Register}/>
      <GuardedRoute exact path='/users' auth={user.isLoggedIn} component={AllUsers}/>
      <GuardedRoute exact path='/users/:id' auth={user.isLoggedIn} component={UserDetails}/>
      <GuardedRoute exact path='/courses' auth={user.isLoggedIn} component={AllCourses}/>
      <Route exact path='/courses/public' component={PublicCourses}/>
      <GuardedRouteTeacher exact path='/courses/new' auth={user.isLoggedIn} teacher={isTeacher} component={CreateCourse}/>
      <GuardedRoute exact path='/courses/:id' auth={user.isLoggedIn} teacher={isTeacher} component={CourseDetails}/>
      <GuardedRouteTeacher exact path='/courses/:id/edit' auth={user.isLoggedIn} teacher={isTeacher} component={EditCourse}/>  
      <GuardedRouteTeacher exact path='/courses/:id/sections/order' auth={user.isLoggedIn} teacher={isTeacher}  component={OrderSections}/> 
      <GuardedRoute exact path='/courses/:courseId/sections/:sectionId' auth={user.isLoggedIn} component={SectionDetails}/>
      <GuardedRouteTeacher exact path='/courses/:id/new' auth={user.isLoggedIn} teacher={isTeacher} component={CreateSectionForm}/>
      <GuardedRouteTeacher exact path='/courses/:courseId/sections/:sectionId/edit' auth={user.isLoggedIn} teacher={isTeacher} component={EditSection}/>
      <GuardedRoute exact path='/users/:id/courses' auth={user.isLoggedIn} component={MyCourses}/>
      <GuardedRouteTeacher exact path='/courses/:id/enroll' auth={user.isLoggedIn} teacher={isTeacher} component={EnrolledStudents}/>

      </Switch>
      </AuthContext.Provider>
    </BrowserRouter>
  );
}

export default App;
