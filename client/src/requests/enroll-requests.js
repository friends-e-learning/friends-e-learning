import { BASE_URL } from "../common/constants";

export const enrollRequest = (id) => {
    return fetch(`${BASE_URL}/courses/${id}/enroll`, {
        method: 'PUT',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
    
        })
        .then(res => res.json());
};

export const enrollTeacherRequest = (id, data) => {
    return fetch(`${BASE_URL}/courses/${id}/enroll`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(data)
    
        })
        .then(res => res.json());
};
export const unenrollRequest = (id) => {
    return fetch(`${BASE_URL}/courses/${id}/enroll`, {
        method: 'DELETE',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
    
        })
        .then(res => res.json());
};

export const unEnrollTeacherRequest = (id, data) => {
    return fetch(`${BASE_URL}/courses/${id}/unenroll`, {
        method: 'DELETE',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(data)
    
        })
        .then(res => res.json());
};

export const getEnrolledStudents = (id, courses_id) => {
    return fetch(`${BASE_URL}/courses/${id}/enrolled/${courses_id}`, {
        method: 'GET',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
    
        })
        .then(res => res.json());
};

export const getNotEnrolledStudents = (id, courses_id) => {
    return fetch(`${BASE_URL}/courses/${id}/unenrolled/${courses_id}`, {
        method: 'GET',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
    
        })
        .then(res => res.json());
};