import { BASE_URL } from "../common/constants";


export const signInUser = (email, password) => {
    return fetch(`${BASE_URL}/auth/signin`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email,
            password,
          })
        })
        .then(res => res.json());
};