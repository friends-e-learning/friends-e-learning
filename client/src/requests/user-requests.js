import { BASE_URL } from "../common/constants";

export const getAllUsers = () => {
    return fetch(`${BASE_URL}/users`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
            }})
            .then(res => res.json());
};

export const getUserById = (id) => {
    return fetch(`${BASE_URL}/users/${id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }})
        .then(res => res.json());
};

export const createUser = (email, firstName, lastName, password) => {
    return fetch(`${BASE_URL}/users`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            email,
            firstName,
            lastName,
            password
          })
        })
        .then(res => res.json());
};

export const editUserRequest = (id, email, firstName, lastName, password) => {
    return fetch(`${BASE_URL}/users/${id}`, {
        method: 'PUT',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(email, firstName, lastName, password)
        })
        .then(res => res.json());
};


export const deleteUserRequest = (id) => {
    return fetch(`${BASE_URL}/users/${id}`, {
        method: 'DELETE',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
    })
    .then(res => res.json());
};

export const getStudentsCoursesRequest = (id) => {
    return fetch(`${BASE_URL}/users/${id}/enrolled`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }})
        .then(res => res.json());
};