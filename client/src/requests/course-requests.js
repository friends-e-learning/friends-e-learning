import { BASE_URL } from "../common/constants";

export const getAllCourses = () => {
    return fetch(`${BASE_URL}/courses`)
        .then(res => res.json());
};

export const getAllPublicCourses = () => {
    return fetch(`${BASE_URL}/courses/public`)
        .then(res => res.json());
};

export const getCourseById = (id) => {
    return fetch(`${BASE_URL}/courses/${id}`, {
        method: 'GET',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        })
        .then(res => res.json());
};

export const createCourseRequest = (data) => {
    return fetch(`${BASE_URL}/courses`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(data)
        })
        .then(res => res.json());
};

export const updateCourse = (id, data) => {
    return fetch(`${BASE_URL}/courses/${id}`, {
        method: 'PUT',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(data)
        })
        .then(res => res.json());
};

export const publicCourseRequest = (id) => {
    return fetch(`${BASE_URL}/courses/${id}/public`, {
        method: 'PUT',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        }})
        .then(res => res.json());
};

export const privateCourseRequest = (id) => {
    return fetch(`${BASE_URL}/courses/${id}/private`, {
        method: 'PUT',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        }})
        .then(res => res.json());
};

export const deleteCourse = (id) => {
    return fetch(`${BASE_URL}/courses/${id}`, {
        method: 'DELETE',
        headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json());
};

