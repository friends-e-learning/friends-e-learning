import { BASE_URL } from "../common/constants";

export const getCourseSections = (id) => {
    return fetch(`${BASE_URL}/courses/${id}/sections`, {
        method: 'GET',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
    })
        .then(res => res.json());
};

export const getSectionsById = (courseId, sectionId) => {
    return fetch(`${BASE_URL}/courses/${courseId}/sections/${sectionId}`, {
        method: 'GET',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        })
        .then(res => res.json());
};

export const createSection = (id, data) => {
    return fetch(`${BASE_URL}/courses/${id}/sections`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(data)
        })
        .then(res => res.json());
};

export const updateSection = (courseId, sectionId, data) => {
    return fetch(`${BASE_URL}/courses/${courseId}/sections/${sectionId}`, {
        method: 'PUT',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(data)
        })
        .then(res => res.json());
};

export const updateSectionOrderUp = (courseId, sectionId, data) => {
    return fetch(`${BASE_URL}/courses/${courseId}/sections/${sectionId}/up`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(data)
        })
        .then(res => res.json());
};

export const updateSectionOrderDown = (courseId, sectionId, data) => {
    return fetch(`${BASE_URL}/courses/${courseId}/sections/${sectionId}/down`, {
        method: 'POST',
        headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(data)
        })
        .then(res => res.json());
};

export const deleteSection = (courseId, sectionId) => {
    return fetch(`${BASE_URL}/courses/${courseId}/sections/${sectionId}`, {
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json());
};
